//
//  AddPartcipantVC.swift
//  My Wall
//
//  Created by surendra on 28/01/21.
//

import UIKit
import FFPopup


protocol AddPartcipantVCDelegate : NSObject {
    func cancelButtonTapped()
    func sendParticipantObject(partcipantObj:[String:String],model:Participant)
}


struct AddparticipantModel {
    let KName : String?
    let KPhonPare : String?
    let kDepartment : String?
    let KDesigantion : String?
}


class AddPartcipantVC: UIViewController {
    
    weak var delegate: AddPartcipantVCDelegate?
    @IBOutlet weak var nameTextfiled: HSUnderLineTextField!
    @IBOutlet weak var phoneTextfuled: HSUnderLineTextField!
    @IBOutlet weak var departmentTextfiled: HSUnderLineTextField!
    @IBOutlet weak var designationTextfiled: HSUnderLineTextField!
    var IsFromUpdate : Bool = false
    var meetingEachEventInfo : ViewMeetingInfoModel? = nil
    var isFromCreateMeeting : Bool = true

    
    @IBOutlet weak var participantTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        nameTextfiled.setCustomPlaceHolder("Name :", fontSize: 16, color: .lightGray)
        phoneTextfuled.setCustomPlaceHolder("Phone :", fontSize: 16, color: .lightGray)
        departmentTextfiled.setCustomPlaceHolder("Department :", fontSize: 16, color: .lightGray)
        designationTextfiled.setCustomPlaceHolder("Designation :", fontSize: 16, color: .lightGray)
        phoneTextfuled.keyboardType = .numberPad
        
        if self.IsFromUpdate == false {
            nameTextfiled.text = ""
            phoneTextfuled.text = ""
            departmentTextfiled.text = ""
            designationTextfiled.text = ""
            nameTextfiled.delegate = self
            phoneTextfuled.delegate = self
            departmentTextfiled.delegate = self
            phoneTextfuled.delegate = self
        }
        
        
        
        
        
    }

    
    @IBAction func cancelTapped(_ sender: Any) {
        self.delegate?.cancelButtonTapped()
    }
    

    func validateFields() -> Bool {
        var validUserName: Bool = true
        var validPassword: Bool = true
        var validDepartment: Bool = true
        var validDesignation: Bool = true

        
        if let userName = nameTextfiled.text {
            validUserName = userName.utf8.count > 0
            if !validUserName {
                nameTextfiled.displayErrorPlaceholder("Please enter Name")
                nameTextfiled.displayError()
            }
        }
        if let userPassword = phoneTextfuled.text {
            validPassword = userPassword.utf8.count > 0
            if !validPassword {
                phoneTextfuled.displayErrorPlaceholder("Please enter Phone Number")
                phoneTextfuled.displayError()
            }
        }
        
        if let userDepartment = departmentTextfiled.text {
            validDepartment = userDepartment.utf8.count > 0
            if !validDepartment {
                departmentTextfiled.displayErrorPlaceholder("Please enter Department")
                departmentTextfiled.displayError()
            }
        }
        
        if let userDesignation = designationTextfiled.text {
            validDesignation = userDesignation.utf8.count > 0
            if !validDesignation {
                designationTextfiled.displayErrorPlaceholder("Please enter Desination")
                designationTextfiled.displayError()
            }
        }
        
        
        return validUserName && validPassword && validDepartment && validDesignation
    }

    
    @IBAction func saveTapped(_ sender: Any) {
        
        if self.isFromCreateMeeting {
            self.view.endEditing(true)
            guard validateFields() else {return}
            guard let userName = nameTextfiled.text else {return}
            guard let phone = phoneTextfuled.text else {return}
            guard let department = departmentTextfiled.text else {return}
            guard let designation = designationTextfiled.text else {return}
            var parameters = ["name":userName, "phone":phone,"dept":department,"designation":designation,"attendanceStatus":"",
                              "meetingObjId":"","id":"null"]
            let parModel = Participant.init(id: "", name: userName, phone: phone, designation: designation, dept: department, attendanceStatus: "", meetingObjID: "")
            self.delegate?.sendParticipantObject(partcipantObj: parameters, model: parModel)
        }
        else{
            
            guard validateFields() else {return}
            guard let userName = nameTextfiled.text else {return}
            guard let phone = phoneTextfuled.text else {return}
            guard let department = departmentTextfiled.text else {return}
            guard let designation = designationTextfiled.text else {return}
            var parameters = ["name":userName, "phone":phone,"dept":department,"designation":designation,"attendanceStatus":"",
                             "meetingObjId":meetingEachEventInfo?.meetingInfo.id ?? "" ,"id":""]
            let parModel = Participant.init(id: "", name: userName, phone: phone, designation: designation, dept: department, attendanceStatus: "", meetingObjID: "")

            let InputUrl = myHelper.createparticipant
            let activityView = MyActivityView()
            activityView.displayLoader()
            ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, parameters, success: { (urlData) in
                activityView.dismissLoader()
                do {
                    let val = String(data: urlData, encoding: String.Encoding.utf8)
                    DispatchQueue.main.async {
                        
                        if ((val?.contains("true")) == true)
                        {
                             parameters = ["name":userName, "phone":phone,"dept":department,"designation":designation,"attendanceStatus":"",
                                           "meetingObjId":self.meetingEachEventInfo?.meetingInfo.id ?? "" ,"id":"null"]

                            self.delegate?.sendParticipantObject(partcipantObj: parameters, model: parModel)

                        }
                    }
                    
                } catch  {
                    do {
                        let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                        ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                    }
                    catch {
                        ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                    }
                }
                
            })
            { (errorString) in
                activityView.dismissLoader()
                ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
            }

            
        }

        
    }
    
  
    
    
    
    

}


extension AddPartcipantVC : UITextFieldDelegate
{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
    }
    
}
