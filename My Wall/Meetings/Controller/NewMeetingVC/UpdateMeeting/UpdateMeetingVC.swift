//
//  NewMeetingVC.swift
//  My Wall
//
//  Created by surendra on 20/12/20.
//

import UIKit
import IQKeyboardManagerSwift
import YYCalendar
import EventKit
import FFPopup

class UpdateMeetingVC: UIViewController,WWCalendarTimeSelectorProtocol,UNUserNotificationCenterDelegate {
    
    var meetingDate : String = ""
    var selectedRowIrem =  0
    var selectedRowIremStatus =  4
    var selectedRowIremStatusSub =  0

    var parentVC = UIViewController()
    var calendarEachEvent : GetMeetingModelInfoElement? = nil
    var meetingEachEventInfo : ViewMeetingInfoModel? = nil

    
    var isfromUpdate : Bool = false
    var initialModel : CreateMeetingInfoModel? = nil
    var selectedDate : String? = ""
    var isfromMettingTypeOffcial : Bool = false
    var participantModelArray : [Participant] = []
    var agendaAmodelArray:[Agenda] = []
    let contentView = AddPartcipantVC(nibName: "AddPartcipantVC", bundle: nil)
    let contentViewAgenda = AddAgendaVC(nibName: "AddAgendaVC", bundle: nil)
    let updateSegDVC = UpdateDeaylsVC(nibName: "UpdateDeaylsVC", bundle: nil)
    let updateParticipantVCObj = UpdateParticipantVC(nibName: "UpdateParticipantVC", bundle: nil)
    
    let updateMomVCObj = UpdateMomVC(nibName: "UpdateMomVC", bundle: nil)


    var isFromMettingSubTypeFollowup : Bool = true
    var participantsArray = [[String:Any]]()
    var agendaArray = [[String:Any]]()
    var popupVC = PopupViewController(contentController: UIViewController(), popupWidth: 100, popupHeight: 100)
    var popupVCUpadate = PopupViewController(contentController: UIViewController(), popupWidth: 100, popupHeight: 100)
    var popupVCUpadateMOM = PopupViewController(contentController: UIViewController(), popupWidth: 100, popupHeight: 100)

    
    var topLabelTtilw  : String? = ""
    var topMeetingTpe  : String? = "Official"
    var topMeetingTpeSub  : String? = "Follow-Up"
    var topMeetingDate  : String? = ""
    var topMeetingStartTime  : String? = ""
    var topMeetingEndTime  : String? = ""
    var addressString  : String? = ""
    var departmentString  : String? = ""
    var meetingTypeStatusString : String = "Scheduled"
    var parentMeetingTitle : String = ""
    var momStatusString : String = "Draft"

    
    var meeetingModel : AllMeetingsModelElementElement?
    

    var typeUpdate : Bool = false
    var timeUpdate : Bool = false
    var startTimeUpdate : Bool = false
    var endTimeUpdate : Bool = false
    var showAddParticipantFuture : Bool = false
    
    var isFromAgenda : Bool = true
    var isfromMom : Bool = false
    var isFromNotes = false
    
    var isAttendedMeeting = false


    var momModelArray:[MOMViewModelElement] = []
    var noteModelArray :[NoteViewElement] = []
    var notesArray = [[String:String]]()
    
    
    var momsArray : [String]? = []
    var momIndex =  1
    var MeetingIDSer : String = ""
    var isMomFulfilled = false
    var isMomApproved = false


    
    lazy var tableview: UITableView = {
        let tableView = UITableView()
        tableView.delegate = (self as UITableViewDelegate)
        tableView.dataSource = (self as UITableViewDataSource)
        tableView.tableFooterView = UIView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    var section1 : [menuItem]!
    var section2 : [menuItem]!
    var section3 : [menuItem]!
    var section4 : [menuItem]!
    var section5 : [menuItem]!
    var section6 : [menuItem]!
    var section7 : [menuItem]!
    var section8 : [menuItem]!
    var section9 : [menuItem]!
    var section10 : [menuItem]!
    var section11 : [menuItem]!

    
    let Title = menuItem(Name: "Title :")
    let Meeting = menuItem(Name: "Meeting Type :")
    let Date = menuItem(Name: "Date :")
    let StartTime = menuItem(Name: "Start Time :")
    let EndTime = menuItem(Name: "End Time :")
    let Address = menuItem(Name: "Location :")
    let Department = menuItem(Name: "Department :")
    let Status = menuItem(Name: "Status :")
    let Createdby = menuItem(Name: "Parent Meeting :")
    let AddParticipants = menuItem(Name: " + Add Participants")
    let Agenda = menuItem(Name: " + Add")
    
    var meetingTypeArray : [String]? = []
    var meetingStatusArray : [String]? = []
    var meetingStatusArraySub : [String]? = []

    
    let chooseDropDownMeetType = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDropDownMeetType
        ]
    }()
    
    let chooseDropDownMeetStatus = DropDown()
    lazy var dropDownStatus: [DropDown] = {
        return [
            self.chooseDropDownMeetStatus
        ]
    }()
    
    
    let chooseDropDownMeetTypeSub = DropDown()
    lazy var dropDownStatusSub: [DropDown] = {
        return [
            self.chooseDropDownMeetTypeSub
        ]
    }()
    
    
    let chooseDropDownMOM = DropDown()
    lazy var dropDownStatusMOM: [DropDown] = {
        return [
            self.chooseDropDownMOM
        ]
    }()
    
    fileprivate var singleDate: Date = NSDate() as Date
    fileprivate var multipleDates: [Date] = []

    let activityView = MyActivityView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Update Meeting"
        
        self.ConfigureNavBarDetilsNewMeeting()
        
        IQKeyboardManager.shared.enable = true
        
        
        section1 = [Title]
        section2 = [Meeting]
        section3 = [Date]
        section4 = [StartTime]
        section5 = [EndTime]
        section6 = [Address]
        section7 = [Department]
        section8 = [Status]
        section9 = [Createdby]
        section10 = [AddParticipants]
        section11 = [Agenda]
        
        //        meetingTypeArray = ["Official","Un Official"]
        //        meetingStatusArray = ["Attended","Cancel","Committed","Rescheduled","Scheduled"]
        
        
        self.topLabelTtilw = self.meetingEachEventInfo?.meetingInfo.title ?? ""
        self.topMeetingTpe = self.meetingEachEventInfo?.meetingInfo.type ?? ""
        self.topMeetingTpeSub = self.meetingEachEventInfo?.meetingInfo.subType ?? ""
        self.topMeetingDate = self.meetingEachEventInfo?.meetingInfo.date ?? ""
        self.topMeetingStartTime = self.meetingEachEventInfo?.meetingInfo.startTime ?? ""
        self.topMeetingEndTime =  self.meetingEachEventInfo?.meetingInfo.endTime ?? ""
        self.addressString = self.meetingEachEventInfo?.meetingInfo.location ?? ""
        self.departmentString =  self.meetingEachEventInfo?.meetingInfo.department ?? ""
        self.meetingTypeStatusString = self.meetingEachEventInfo?.meetingInfo.status ?? ""
        self.parentMeetingTitle = self.meetingEachEventInfo?.meetingInfo.parentMeetingTitle ?? ""
        self.momStatusString = self.meetingEachEventInfo?.meetingInfo.momStatus ?? ""
        
        self.participantModelArray = self.meetingEachEventInfo?.participants ?? []
        self.agendaAmodelArray = self.meetingEachEventInfo?.agendas ?? []
        
        let x : UInt32 = calendarEachEvent?.meetingID ?? 0
        let meetingID = String(x)
        self.meetingDate = meetingID
        
        print(UserManager.sharedInstance.currentUser?.accessToken)
        
        
        if self.meetingEachEventInfo?.meetingInfo.status.contains("Attended") == true {
            self.isAttendedMeeting = true
        }
        
        if self.meetingEachEventInfo?.meetingInfo.momStatus?.contains("Approved") == true {
            self.isMomApproved = true
        }
        
        if self.meetingEachEventInfo?.meetingInfo.momStatus?.contains("Fulfilled") == true {
            self.isMomFulfilled = true
        }
        
        if self.topMeetingTpe?.lowercased() == "official" {
            self.isfromMettingTypeOffcial = true
            if self.topMeetingTpeSub?.lowercased().contains("main") == true {
                self.isFromMettingSubTypeFollowup = false
            }else{
                self.isFromMettingSubTypeFollowup = true
            }
        }else{
            self.isfromMettingTypeOffcial = false
            self.isFromMettingSubTypeFollowup = false
        }
        
        
        let dateFormatterss = DateFormatter()
        dateFormatterss.dateFormat = "HH:mm:ss"
        let dateinPm = dateFormatterss.date(from: (self.calendarEachEvent?.endTime)!)
        dateFormatterss.dateFormat = "HH:mm"
        let dateStandaradstartTime = dateFormatterss.string(from: dateinPm!)
        let eventStartTime = (self.calendarEachEvent?.date)! + " " + (dateStandaradstartTime)
        let eentDateFormater = DateFormatter()
        eentDateFormater.dateFormat = "yyyy-MM-dd HH:mm"
        eentDateFormater.locale = NSLocale.current
        let serverdate = eentDateFormater.date(from: eventStartTime)!
        let currentDate = NSDate()
        if serverdate.compare(currentDate as Date) == ComparisonResult.orderedDescending {
            //server date is grater
            self.showAddParticipantFuture = true
        }else{
            self.showAddParticipantFuture = false
        }
        

        

        view.addSubview(tableview)
        tableview.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        tableview.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        tableview.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        tableview.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        let nibName = UINib(nibName: "MeetDropDownCell", bundle:nil)
        tableview.register(nibName, forCellReuseIdentifier: "MeetDropDownCell")
        
        let nibNameTextEditCell = UINib(nibName: "TextEditCell", bundle:nil)
        tableview.register(nibNameTextEditCell, forCellReuseIdentifier: "TextEditCell")
        
        let nibNameMeetTextViewCell = UINib(nibName: "MeetTextViewCell", bundle:nil)
        tableview.register(nibNameMeetTextViewCell, forCellReuseIdentifier: "MeetTextViewCell")
        
        let addParticipantCell = UINib(nibName: "AddParticipantCell", bundle:nil)
        tableview.register(addParticipantCell, forCellReuseIdentifier: "AddParticipantCell")
        
        let addAgendaplus = UINib(nibName: "AddAgendaPlusCell", bundle:nil)
        tableview.register(addAgendaplus, forCellReuseIdentifier: "AddAgendaPlusCell")

        let addParticipantInfoCell = UINib(nibName: "AddParticipantInfoCell", bundle:nil)
        tableview.register(addParticipantInfoCell, forCellReuseIdentifier: "AddParticipantInfoCell")

        let addAgendaCell = UINib(nibName: "AddAgendaCell", bundle:nil)
        tableview.register(addAgendaCell, forCellReuseIdentifier: "AddAgendaCell")

        let MeetDropDownCellSubTypeNib = UINib(nibName: "MeetDropDownCellSubType", bundle:nil)
        tableview.register(MeetDropDownCellSubTypeNib, forCellReuseIdentifier: "MeetDropDownCellSubType")
        
        let SaveUpdatedCellNib = UINib(nibName: "SaveUpdatedCell", bundle:nil)
        tableview.register(SaveUpdatedCellNib, forCellReuseIdentifier: "SaveUpdatedCell")
        
        let ViewNoteCellNib = UINib(nibName: "ViewNoteCell", bundle:nil)
        tableview.register(ViewNoteCellNib, forCellReuseIdentifier: "ViewNoteCell")
        
        
        let UpdateParticipantInfoCellNib = UINib(nibName: "UpdateParticipantInfoCell", bundle:nil)
        tableview.register(UpdateParticipantInfoCellNib, forCellReuseIdentifier: "UpdateParticipantInfoCell")

        
        let MomCellNib = UINib(nibName: "MomCell", bundle:nil)
        tableview.register(MomCellNib, forCellReuseIdentifier: "MomCell")
        
        
        let MomSegCellMIn = UINib(nibName: "MomSegCell", bundle:nil)
        tableview.register(MomSegCellMIn, forCellReuseIdentifier: "MomSegCell")

        
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = 60

        
        tableview.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        
//        var customView = MeetingFooterViewWall()
//        customView = MeetingFooterViewWall(frame: CGRect(x: 0, y: 20, width: tableview.frame.width, height: 80))
//        tableview.tableFooterView = customView
//        customView.save.setTitle("Update", for: .normal)
//        customView.cancel.addTarget(self, action: #selector(cancelAcrtion), for: .touchUpInside)
//        customView.save.addTarget(self, action: #selector(saveMettingInfo), for: .touchUpInside)
        
        
        
    }
    
    
    
    func refreshMettingInfo()
    {
        guard let token:String = UserManager.sharedInstance.currentUser?.accessToken else {
            return
        }
        let x : UInt32 = calendarEachEvent?.meetingID ?? 0
        let meetingID = String(x)
        
        let catUrl : String  =  myHelper.ViewUpdateMeeting! + meetingID
        activityView.displayLoader()
        let parameters: [String: String] = ["access_token":token]
        ApiService.sharedManager.getServiceWithoutToken(catUrl, parameters) { (urlData) in
            self.activityView.dismissLoader()
            do {
                let rootDicnew = try JSONDecoder().decode(ViewMeetingInfoModel.self, from: urlData)
                self.meetingEachEventInfo = rootDicnew
                print("")
                
                self.topLabelTtilw = self.meetingEachEventInfo?.meetingInfo.title ?? ""
                self.topMeetingTpe = self.meetingEachEventInfo?.meetingInfo.type ?? ""
                self.topMeetingTpeSub = self.meetingEachEventInfo?.meetingInfo.subType ?? ""
                self.topMeetingDate = self.meetingEachEventInfo?.meetingInfo.date ?? ""
                self.topMeetingStartTime = self.meetingEachEventInfo?.meetingInfo.startTime ?? ""
                self.topMeetingEndTime =  self.meetingEachEventInfo?.meetingInfo.endTime ?? ""
                self.addressString = self.meetingEachEventInfo?.meetingInfo.location ?? ""
                self.departmentString =  self.meetingEachEventInfo?.meetingInfo.department ?? ""
                self.meetingTypeStatusString = self.meetingEachEventInfo?.meetingInfo.status ?? ""
                self.parentMeetingTitle = self.meetingEachEventInfo?.meetingInfo.parentMeetingTitle ?? ""
                self.momStatusString = self.meetingEachEventInfo?.meetingInfo.momStatus ?? ""
                
                self.participantModelArray = self.meetingEachEventInfo?.participants ?? []
                self.agendaAmodelArray = self.meetingEachEventInfo?.agendas ?? []

                
                self.tableview.reloadData()
                
            }
            catch (let error) {
                self.activityView.dismissLoader()
                do {
                    self.view.makeToast(error.localizedDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "", subTitle: error.localizedDescription)
                }
            }
            
        }
        failure: { (errorString) in
            self.activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
        
        
    }
    

    
    
    func ConfigureNavBarDetilsNewMeeting(){
        navigationController?.navigationBar.barTintColor = UIColor.MyWall.appColor
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        backButton.setImage(UIImage(named: "arrow"), for: .normal)
        backButton.addTarget(self, action: #selector(backActionDetailsMeeting), for: .touchUpInside)
        backButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        
        let rightbackButton = UIButton(type: .custom)
        rightbackButton.setImage(UIImage.init(named: "ktr30"), for: .normal)
        rightbackButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightbackButton.layer.cornerRadius = 15
        rightbackButton.clipsToBounds = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightbackButton)
        
    }
    
    @objc func backActionDetailsMeeting () {
        if self.parentVC.isKind(of: SwiftDemoViewController.self) {
            let healthDataVC : SwiftDemoViewController = self.parentVC as! SwiftDemoViewController
            healthDataVC.selectedDate = ""
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UNUserNotificationCenter.current().delegate = self
        self.getMeetingDropDownInfo()
    }
    
    func getMeetingDropDownInfo(){
        guard let token:String = UserManager.sharedInstance.currentUser?.accessToken else {
            return
        }
        let catUrl : String  =  myHelper.getmeetinginitData
        let activityView = MyActivityView()
        activityView.displayLoader()
        let parameters: [String: String] = ["access_token":token]
        ApiService.sharedManager.getServiceWithoutToken(catUrl, parameters) { (urlData) in
            activityView.dismissLoader()
            do {
                let rootDicnew = try JSONDecoder().decode(CreateMeetingInfoModel.self, from: urlData)
                self.initialModel = rootDicnew
                
                if rootDicnew.typesList!.count > 0{
                    self.meetingTypeArray = rootDicnew.typesList?.compactMap { $0.listItem}


                    
                  /*  var filterArrayType : [String] = []
                    filterArrayType = (rootDicnew.typesList?.filter({ $0.status == "Active"}).compactMap { $0.listItem})! */
                }
                if rootDicnew.statusList!.count > 0{
                    self.meetingStatusArray = rootDicnew.statusList?.compactMap { $0.listItem }
                    /*  var filterArrayStatus : [String] = []
                    filterArrayStatus = (rootDicnew.statusList?.filter({ $0.status == "Active"}).compactMap { $0.listItem})!
                    print(filterArrayStatus) */
                }
                
                if rootDicnew.meetingSubTypeList!.count > 0 {
                    self.meetingStatusArraySub = rootDicnew.meetingSubTypeList?.compactMap { $0.listItem }
                }
                
                if rootDicnew.momStatusList!.count > 0 {
                    self.momsArray = rootDicnew.momStatusList?.compactMap { $0.listItem }
                }
                
                self.tableview.reloadData()
            }
            catch (let error) {
                activityView.dismissLoader()
                do {
                    self.view.makeToast(error.localizedDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "", subTitle: error.localizedDescription)
                }
            }
            
        }
        failure: { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
            
        }
    }
    
    
    
    
    @objc func cancelAcrtion() {
        self.backActionDetailsMeeting()
    }
    
    
    
    func validateFields() -> Bool {
        var validTtile: Bool = true
        var validAddress: Bool = true
        var validDepartment: Bool = true
        
        if let userName = self.topLabelTtilw {
            validTtile = userName.utf8.count > 0
            if !validTtile {
                let indexPathTtilw = IndexPath(row: 0, section: 0)
                self.tableview.scrollToRow(at: indexPathTtilw, at: .none, animated: true)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    let cell : TextEditCell = self.tableview.cellForRow(at: indexPathTtilw)! as! TextEditCell
                    cell.titleDescription.becomeFirstResponder()
                    cell.titleDescription.displayErrorPlaceholder("Please enter Ttile ")
//                    cell.titleDescription.displayError()
                }
            }
        }
        
        if let address = addressString {
            validAddress = address.utf8.count > 0
            if !validAddress {
                ErrorManager.showErrorAlert(mainTitle: "", subTitle: "Please enter  Address", withDuration: 0.5)
            }
        }
        
        if let department = departmentString {
            validDepartment = department.utf8.count > 0
            if !validDepartment {
                ErrorManager.showErrorAlert(mainTitle: "", subTitle: "Please enter Department", withDuration: 0.5)
            }
        }
        
        return validTtile && validAddress && validDepartment
    }
    
    
    
    
    @objc func saveMettingInfo() {
        
        self.view.endEditing(true)
        guard validateFields() else {return}
        let dateFormatter = DateFormatter()

        var dateFinal : String = ""
        var dateStandaradstartTime : String = ""
        var dateEndTime : String = ""
        
        if self.timeUpdate {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "MM/dd/yyyy"
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let dateObj: Date? = dateFormatterGet.date(from: self.topMeetingDate!)
             dateFinal = dateFormatter.string(from: dateObj!)
        }else{
            dateFinal = self.topMeetingDate ?? ""
        }
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "hHH:mm:ss"
        var someDate = self.topMeetingStartTime!
        if dateFormatterGet.date(from: someDate) != nil {
            dateFormatter.dateFormat = "HH:mm:ss"
            let dateinPm = dateFormatter.date(from: (self.topMeetingStartTime)!)
            dateFormatter.dateFormat = "HH:mm"
            dateStandaradstartTime = dateFormatter.string(from: dateinPm!)

        } else {
            dateFormatter.dateFormat = "hh:mm a"
            let dateinPm = dateFormatter.date(from: self.topMeetingStartTime!)
            dateFormatter.dateFormat = "HH:mm"
            dateStandaradstartTime = dateFormatter.string(from: dateinPm!)
        }
        
        
        dateFormatterGet.dateFormat = "hHH:mm:ss"
        someDate = self.topMeetingEndTime!
        if dateFormatterGet.date(from: someDate) != nil {
            dateFormatter.dateFormat = "HH:mm:ss"
            let dateinPm = dateFormatter.date(from: (self.topMeetingEndTime)!)
            dateFormatter.dateFormat = "HH:mm"
            dateEndTime = dateFormatter.string(from: dateinPm!)
        } else {
            dateFormatter.dateFormat = "hh:mm a"
            let dateendTime = dateFormatter.date(from: self.topMeetingEndTime!)
            dateFormatter.dateFormat = "HH:mm"
            dateEndTime = dateFormatter.string(from: dateendTime!)
        }
        
        
//        if self.startTimeUpdate {
//            dateFormatter.dateFormat = "hh:mm a"
//            let dateinPm = dateFormatter.date(from: self.topMeetingStartTime!)
//            dateFormatter.dateFormat = "HH:mm"
//            dateStandaradstartTime = dateFormatter.string(from: dateinPm!)
//        }else{
//            dateFormatter.dateFormat = "HH:mm:ss"
//            let dateinPm = dateFormatter.date(from: (self.topMeetingStartTime)!)
//            dateFormatter.dateFormat = "HH:mm"
//            dateStandaradstartTime = dateFormatter.string(from: dateinPm!)
//        }
        
        
//        if self.endTimeUpdate {
//            dateFormatter.dateFormat = "hh:mm a"
//            let dateendTime = dateFormatter.date(from: self.topMeetingEndTime!)
//            dateFormatter.dateFormat = "HH:mm"
//            dateEndTime = dateFormatter.string(from: dateendTime!)
//
//        }else{
//            dateFormatter.dateFormat = "HH:mm:ss"
//            let dateinPm = dateFormatter.date(from: (self.topMeetingEndTime)!)
//            dateFormatter.dateFormat = "HH:mm"
//            dateEndTime = dateFormatter.string(from: dateinPm!)
//        }
        
        
        
        
        
        let eventStartTime = dateFinal + " " + dateStandaradstartTime
        let eventEndTime = dateFinal + " " + dateEndTime
        let eentDateFormater = DateFormatter()
        eentDateFormater.dateFormat = "yyyy-MM-dd HH:mm"
        eentDateFormater.locale = NSLocale.current
        
        let eeventStartDate = eentDateFormater.date(from: eventStartTime)!
        let eventEndDate : Date = eentDateFormater.date(from: eventEndTime) ?? NSDate() as Date
        
        
        let id : String = self.meetingEachEventInfo?.meetingInfo.id ?? ""
        let meetingId = self.meetingEachEventInfo?.meetingInfo.meetingID ?? 0
        let title = self.topLabelTtilw ?? ""
        let type = self.topMeetingTpe ?? ""
        let subType = self.topMeetingTpeSub ?? ""
        let date = dateFinal
        let startTime = dateStandaradstartTime
        let endTime = dateEndTime
        let location = addressString
        let department = departmentString
        let status = self.meetingTypeStatusString
        let createdBy = UserManager.sharedInstance.currentUserInfomation?.user.username ?? ""
        
        
        let parameters = ["id":id,
                          "meetingId":meetingId,
                          "title":title,
                          "type":type,
                          "subType":subType,
                          "status":status,
                          "date":date,
                          "startTime":startTime,
                          "endTime":endTime,
                          "location":location ?? "",
                          "department":department ?? "",
                          "momStatus":momStatusString,
                          "createdBy":createdBy,
                          "parentMeetingTitle":self.parentMeetingTitle ,
                          "parentMeetingId": self.meeetingModel?.meetingID ?? 0,
                          "parentMeetingObjId": self.meeetingModel?.id ?? ""
        ] as [String : Any]
        let agenda = status
        
        /*
        for par : Participant in self.participantModelArray {
            var dictionary: [String: Any]  =  par.dictionary ??  [:]
            self.participantsArray.append(dictionary)
        }
        for aggenda : Agenda in self.agendaAmodelArray {
            var dictionaryAgenda: [String: Any]  =  aggenda.dictionary ??  [:]
            self.agendaArray.append(dictionaryAgenda)
        }
        let agendas : [String : Any] = ["agendas" : self.agendaArray,
                                "createdByName":"",
                                "updatedTime":"",
                                "meetingObjId":"",
                                "notes": ""]
        let endorseDict: [String: Any] = ["meetingInfo": parameters
                                          , "agendaCreateData" : agendas,
                                          "participants" : self.participantsArray,
                                          "userId":createdBy] */
        
        let InputUrl = myHelper.updateMeeting
        let activityView = MyActivityView()
        activityView.displayLoader()
        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, parameters, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.createLocalNotification(title: title, description: agenda, startDate: eeventStartDate , endDate: eventEndDate, location: location)
                        
                        self.addEventToCalendar(title: title, description: agenda, startDate:  eeventStartDate, endDate:  eventEndDate, location: location) {
                            (sucess, error) in
                        }
                        
                        if self.isfromUpdate {
                            ErrorManager.showSuccessAlert(mainTitle: "Meeting Updated successfully", subTitle: "")
                        }else{
                            ErrorManager.showSuccessAlert(mainTitle: "Meeting created successfully", subTitle: "")
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
    }
    
    
    func createLocalNotification(title: String, description: String, startDate: Date, endDate: Date, location: String?)
    {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: title, arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: description, arguments: nil)
        content.sound = UNNotificationSound.default
        content.badge = 1
        content.categoryIdentifier = "alarm"


        
        let today = startDate
//        let today : Date = NSDate() as Date
        let triggerWeekly = Calendar.current.dateComponents([.day,.hour,.minute,.second,], from: today)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)

//        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComp, repeats: true)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
    }
    
    
    func addEventToCalendar(title: String, description: String?, startDate: Date, endDate: Date, location: String?, completion: ((_ success: Bool, _ error: NSError?) -> Void)? = nil) {
        DispatchQueue.global(qos: .background).async { () -> Void in
            let eventStore = EKEventStore()
            
            eventStore.requestAccess(to: .event, completion: { (granted, error) in
                if (granted) && (error == nil) {
                    let alarm = EKAlarm(relativeOffset: -3600.0)
                    let event = EKEvent(eventStore: eventStore)
                    event.title = title
                    event.startDate = startDate
                    event.endDate = endDate
                    event.notes = description
                    event.alarms = [alarm]
                    event.location = location
                    event.calendar = eventStore.defaultCalendarForNewEvents
                    event.addAlarm(EKAlarm(absoluteDate: event.startDate))
                    event.addAlarm(alarm)

                    do {
                        try eventStore.save(event, span: .thisEvent,commit: true)
                    } catch let e as NSError {
                        completion?(false, e)
                        print ("\(#file) - \(#function) error: \(e.localizedDescription)")
                        return
                    }
                    completion?(true, nil)
                } else {
                    completion?(false, error as NSError?)
                    print ("\(#file) - \(#function) error: \(error)")
                }
            })
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
           completionHandler([.alert,.sound])
        
        
       }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
           
           
       }
    
    
    
    @objc  func calnderClicked() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let createDateObj = dateFormatter.string(from: NSDate() as Date)
        let indexPath = IndexPath(row: 0, section: 3)
        let cell : MeetDropDownCell = self.tableview.cellForRow(at: indexPath)! as! MeetDropDownCell

        let calendar = YYCalendar(limitedCalendarLangType: .KOR,
                                  date: createDateObj,
                                  minDate: createDateObj,
                                  maxDate: "02/02/2030",
                                  format: "MM/dd/yyyy") { [weak self] date in
            
            cell.meetTextField.text = date
            self?.topMeetingDate = date
            self?.timeUpdate = true
        }

        calendar.dayButtonStyle = DayButtonStyle.circle
        calendar.dimmedBackgroundAlpha = 0.3
        calendar.headerViewBackgroundColor = #colorLiteral(red: 0.6385190487, green: 0.4598317742, blue: 0.2793223262, alpha: 1)
        calendar.show()

        
    }

    @objc  func startTimeClicked() {
        
        let selector:WWCalendarTimeSelector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: Bundle.main).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        selector.delegate = self
        selector.view.tag = 123456
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        selector.optionLayoutHeight = 500

        present(selector, animated: true, completion: nil)

        
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        
        if selector.view.tag == 123456 {
            print("Selected \n\(date)\n---")
            singleDate = date
            let indexPath = IndexPath(row: 0, section: 4)
            let cell : MeetDropDownCell = self.tableview.cellForRow(at: indexPath)! as! MeetDropDownCell
            cell.meetTextField.text =  date.stringFromFormat("h:mm a")
            self.topMeetingStartTime =  date.stringFromFormat("h:mm a")
            self.startTimeUpdate = true
            
            
            let currentDateTime = date
            let date = currentDateTime.addingTimeInterval(60 * 60)
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            formatter.dateStyle = .none
            formatter.string(from: date)
            let createDateObj = formatter.string(from: date)
            
            let indexPath4 = IndexPath(row: 0, section: 5)
            let cell4 : MeetDropDownCell = self.tableview.cellForRow(at: indexPath4)! as! MeetDropDownCell
            cell4.meetTextField.text =  createDateObj
            self.topMeetingEndTime = createDateObj
            self.endTimeUpdate = true
        }
        
        
        if selector.view.tag == 1234567 {
            print("Selected \n\(date)\n---")
            singleDate = date
            let indexPath = IndexPath(row: 0, section: 5)
            let cell : MeetDropDownCell = self.tableview.cellForRow(at: indexPath)! as! MeetDropDownCell
            cell.meetTextField.text =  date.stringFromFormat("h:mm a")
            
            let indexPathfromdate = IndexPath(row: 0, section: 4)
            let cellSection3Fromdate : MeetDropDownCell = self.tableview.cellForRow(at: indexPathfromdate)! as! MeetDropDownCell
            let fromString : String = cellSection3Fromdate.meetTextField.text ?? ""
            let toString : String = date.stringFromFormat("h:mm a")
            let dateDiff = findDateDiff(time1Str: fromString, time2Str: toString)
            if dateDiff.contains("-") {
                self.view.makeToast("Please select valid end time", duration: 3.0, position: .top)
                cell.meetTextField.text = fromString
            }
            self.topMeetingEndTime = cell.meetTextField.text
            self.endTimeUpdate = true

        }
        
    }
    
    func findDateDiff(time1Str: String, time2Str: String) -> String {
        let timeformatter = DateFormatter()
        timeformatter.dateFormat = "hh:mm a"

        guard let time1 = timeformatter.date(from: time1Str),
            let time2 = timeformatter.date(from: time2Str) else { return "" }

        //You can directly use from here if you have two dates

        let interval = time2.timeIntervalSince(time1)
        let hour = interval / 3600;
        let minute = interval.truncatingRemainder(dividingBy: 3600) / 60
        let intervalInt = Int(interval)
        return "\(intervalInt < 0 ? "-" : "+") \(Int(hour)) Hours \(Int(minute)) Minutes"
    }
    
    
    
    
    @objc  func endTimeClicked() {
        
        let selector:WWCalendarTimeSelector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: Bundle.main).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        selector.delegate = self
        selector.view.tag = 1234567
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        selector.optionLayoutHeight = 500
        present(selector, animated: true, completion: nil)

    }
    
    
        
    @objc  func dropdownClicked() {
        self.setupChooseDropDown()
        self.customizeDropDown(self)
        chooseDropDownMeetType.show()
    }
    
    @objc  func dropdownClickedStatus() {
        self.setupChooseDropDownForMettingStatus()
        self.customizeDropDown(self)
        chooseDropDownMeetStatus.show()
    }
    
    
    @objc  func dropdownClickedSubType() {
        self.setupChooseDropDownForMettingStatusSub()
        self.customizeDropDown(self)
        chooseDropDownMeetTypeSub.show()
    }
    
    
    @objc  func dropdownClickedMOM() {
        self.setupChooseDropDownForMOM()
        self.customizeDropDown(self)
        chooseDropDownMOM.show()
    }
    
    @objc  func gtoAllMeetings() {
        let meetingVC = AllMeetingsVC()
        meetingVC.delegate = self
        let aObjNavi = UINavigationController(rootViewController: meetingVC)
        aObjNavi.modalPresentationStyle = .fullScreen
        self.present(aObjNavi, animated: true, completion: nil)
    }
    
}


extension UpdateMeetingVC
{
    func setupChooseDropDown() {
        let indexPath = IndexPath(row: 0, section: 1)
        let cell : MeetDropDownCell = self.tableview.cellForRow(at: indexPath)! as! MeetDropDownCell
        cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI));
        chooseDropDownMeetType.anchorView = cell.meetTextField
        chooseDropDownMeetType.bottomOffset = CGPoint(x: 0, y: cell.meetTextField.bounds.height)
        chooseDropDownMeetType.dataSource = self.meetingTypeArray!
        chooseDropDownMeetType.selectRow(selectedRowIrem)
        chooseDropDownMeetType.selectionAction = { [weak self] (index, item) in
            self!.selectedRowIrem = index
            cell.meetTextField.text = item
            self?.topMeetingTpe = item
            self?.typeUpdate = true
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            if self?.topMeetingTpe?.lowercased() == "official" {
                self?.isfromMettingTypeOffcial = true
                self?.isFromMettingSubTypeFollowup = true
            }else{
                self?.isfromMettingTypeOffcial = false
                self?.isFromMettingSubTypeFollowup = false
            }
            self?.tableview.reloadData()
        }
        chooseDropDownMeetType.cancelAction =  {
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
    }
    
    
    func customizeDropDown(_ sender: AnyObject) {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor.MyWall.LightBlue
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        if #available(iOS 11.0, *) {
            appearance.setupMaskedCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        }
    }
    
    
    func setupChooseDropDownForMettingStatus() {
        let indexPath = IndexPath(row: 0, section: 8)
        let cell : MeetDropDownCell = self.tableview.cellForRow(at: indexPath)! as! MeetDropDownCell
        cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI));
        chooseDropDownMeetStatus.anchorView = cell.meetTextField
        chooseDropDownMeetStatus.bottomOffset = CGPoint(x: 0, y: cell.meetTextField.bounds.height)
        chooseDropDownMeetStatus.dataSource = self.meetingStatusArray!
        chooseDropDownMeetStatus.selectRow(selectedRowIremStatus)
        chooseDropDownMeetStatus.selectionAction = { [weak self] (index, item) in
            self!.selectedRowIremStatus = index
            cell.meetTextField.text = item
            self?.meetingTypeStatusString = item
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
        chooseDropDownMeetStatus.cancelAction =  {
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
    }
    
    func setupChooseDropDownForMettingStatusSub() {
        let indexPath = IndexPath(row: 0, section: 2)
        let cell : MeetDropDownCellSubType = self.tableview.cellForRow(at: indexPath)! as! MeetDropDownCellSubType
        cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI));
        chooseDropDownMeetTypeSub.anchorView = cell.meetTextField
        chooseDropDownMeetTypeSub.bottomOffset = CGPoint(x: 0, y: cell.meetTextField.bounds.height)
        chooseDropDownMeetTypeSub.dataSource = self.meetingStatusArraySub!
        chooseDropDownMeetTypeSub.selectRow(selectedRowIremStatusSub)
        chooseDropDownMeetTypeSub.selectionAction = { [weak self] (index, item) in
            self!.selectedRowIremStatusSub = index
            cell.meetTextField.text = item
            self?.topMeetingTpeSub = item
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
            if self?.topMeetingTpeSub?.lowercased().contains("follow") == true {
                self?.isFromMettingSubTypeFollowup = true
                self?.isfromMettingTypeOffcial = true
            }else{
                self?.isFromMettingSubTypeFollowup = false
            }
            self?.tableview.reloadData()
        }
        chooseDropDownMeetTypeSub.cancelAction =  {
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
    }
    
    
    func setupChooseDropDownForMOM() {
        let indexPath = IndexPath(row: 0, section: 9)
        let cell : MomCell = self.tableview.cellForRow(at: indexPath)! as! MomCell
        cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI));
        chooseDropDownMOM.anchorView = cell.meetTextField
        chooseDropDownMOM.bottomOffset = CGPoint(x: 0, y: cell.meetTextField.bounds.height)
        chooseDropDownMOM.dataSource = self.momsArray!
        chooseDropDownMOM.selectRow(momIndex)
        chooseDropDownMOM.selectionAction = { [weak self] (index, item) in
            self!.momIndex = index
            cell.meetTextField.text = item
            self?.momStatusString = item
            self?.tableview.reloadSections([9], with: .none)
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
        chooseDropDownMeetTypeSub.cancelAction =  {
            cell.meetTextField.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
    }
    
    
    @objc func addParticipantsItem(){
        
        var tempFrame  = contentView.view.frame
        tempFrame.size.width = self.view.frame.size.width - 40
        contentView.view.frame = tempFrame
        popupVC = PopupViewController(contentController: contentView, popupWidth: tempFrame.size.width, popupHeight: tempFrame.size.height)
        popupVC.cornerRadius = 5
        contentView.delegate = self
        contentView.IsFromUpdate = false
        contentView.isFromCreateMeeting = false
        contentView.meetingEachEventInfo = self.meetingEachEventInfo
        contentView.participantTitle.text = "Add Participants"
        present(popupVC, animated: true, completion: nil)
        
    }
    
    @objc func addAgendaItem(){
        
        var tempFrame  = contentView.view.frame
        tempFrame.size.width = self.view.frame.size.width - 40
        tempFrame.size.height = 200
        contentViewAgenda.view.frame = tempFrame
        
        let popup = FFPopup(contetnView: contentViewAgenda.view, showType: .bounceIn, dismissType: .shrinkOut, maskType: .dimmed, dismissOnBackgroundTouch: true, dismissOnContentTouch: false)
        contentViewAgenda.delegate = self
        contentViewAgenda.isFromCreateMeeting = false
        contentViewAgenda.meetingEachEventInfo =  meetingEachEventInfo
        if self.isFromAgenda {
            if self.isAttendedMeeting {
                return
            }
            
            contentViewAgenda.textView.text = "Agenda"
            contentViewAgenda.segTypeLabel.text = "Add Agenda"
            contentViewAgenda.segtype = .agendaSeg
        }
        if self.isfromMom {
            if self.isMomFulfilled {
                return
            }
            if self.isMomApproved {
                return
            }
            contentViewAgenda.textView.text = "MOM's"
            contentViewAgenda.segTypeLabel.text = "Add MOM's"
            contentViewAgenda.segtype = .momSeg
        }
        if self.isFromNotes {
            if self.isAttendedMeeting {
                return
            }
            contentViewAgenda.textView.text = "Notes"
            contentViewAgenda.segTypeLabel.text = "Add Notes"
            contentViewAgenda.segtype = .noteSeg
        }
        
        let layout = FFPopupLayout(horizontal: .center, vertical: .center)
        popup.show(layout: layout)
    }

    
}


extension UpdateMeetingVC : AddPartcipantVCDelegate {
    
    func sendParticipantObject(partcipantObj:[String:String],model:Participant){
        popupVC.dismiss(animated: true, completion: nil)
        self.participantModelArray.append(model)
        DispatchQueue.main.async {
            self.refreshMettingInfo()
        }
    }

    func cancelButtonTapped() {
        popupVC.dismiss(animated: true, completion: nil)
    }
    
}

extension UpdateMeetingVC : AddAgendaVCDelegate {
    
    func cancelAgendaVC() {
        FFPopup.dismiss(contentView: contentViewAgenda.view, animated: true)

        
    }
    func sendAgendaObject(agendaObj:[String:String],agendaModel:Agenda) {
        self.agendaArray.append(agendaObj)
        FFPopup.dismiss(contentView: contentViewAgenda.view, animated: true)
        DispatchQueue.main.async {
            self.refreshMettingInfo()
        }
    }
    
    func closeCreatioNotesAndRfreshNotes(){
        FFPopup.dismiss(contentView: contentViewAgenda.view, animated: true)
        self.isFromAgenda  = false
        self.isfromMom = false
        self.isFromNotes = true
        self.getNotes()
    }
    
    func closeMomandRefresh(){
        
        FFPopup.dismiss(contentView: contentViewAgenda.view, animated: true)
        self.isFromNotes = false
        self.isFromAgenda  = false
        self.isfromMom = true
        self.getMomObjects()
        
    }
    
    

}

extension UpdateMeetingVC : AllMeetingsVCDelegate {
    
    func sendMeetingObjectIDinfo( meeetingModel : AllMeetingsModelElementElement) {
        self.meeetingModel = meeetingModel
        self.parentMeetingTitle = self.meeetingModel?.title ?? ""
    }

}


extension UpdateMeetingVC: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 16
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 2 {
            if self.isfromMettingTypeOffcial {
                return 1
            }else{
                return 0
            }
        }
        
        if section == 9 {
            if self.showAddParticipantFuture {
                return 0
            }else {
                return 1
            }
        }
        
        
        
        if section == 10 {
            if self.isfromMettingTypeOffcial && self.isFromMettingSubTypeFollowup {
                return 1
            }else{
                return 0
            }
        }
        
        if section == 12 {
            if self.showAddParticipantFuture {
                return 1
            }else {
                return 0
            }
        }
        
        
        
        
        if section == 13 {
            if (self.participantModelArray.count > 0) {
                return self.participantModelArray.count
            }else{
                return 0
            }
        }
        
        
        if section == 14 {
            if self.showAddParticipantFuture { //future
                if self.isfromMom {
                    return 0
                }
                return 1
            }else {
                if self.isfromMom  {
                    if self.isMomFulfilled || self.isMomApproved {
                        return 0
                    }
                    return 1
                }
                if self.isFromNotes {
                    return 1
                }
                return 0
            }
        }
        
        
        if section == 15 {
            
            if self.isFromAgenda {
                if (self.agendaAmodelArray.count > 0) {
                    return self.agendaAmodelArray.count
                }else{
                    return 0
                }
            }
            
            if self.isfromMom {
                if (self.momModelArray.count > 0) {
                    return self.momModelArray.count
                }else{
                    return 0
                }
            }
            
            if self.isFromNotes {
                if (self.noteModelArray.count > 0) {
                    return self.noteModelArray.count
                }else{
                    return 0
                }
            }
            
            
        }
        return 1
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 1 {
            self.topLabelTtilw = textField.text
        }
        if textField.tag == 2 {
            self.addressString = textField.text
        }
        if textField.tag == 3 {
            self.departmentString = textField.text
        }
        
    }

    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        var items : [menuItem]!

        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextEditCell", for: indexPath) as! TextEditCell
            cell.titleDescription.tag = 1
            cell.titleDescription.delegate = self
            cell.titleDescription.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                                            for: UIControl.Event.editingChanged)
            cell.selectionStyle = .none
            items = section1
            let theMenuItem = items[indexPath.item]
            cell.titleLabel.text = theMenuItem.Name
            cell.titleDescription.placeholder = "Enter Ttile"
            cell.titleDescription.delegate = self
            cell.titleDescription.text = topLabelTtilw
            if self.isAttendedMeeting {
                cell.isUserInteractionEnabled = false
            }else{
                cell.isUserInteractionEnabled = true
            }
            return cell
        }
        if indexPath.section == 1 {
            var cell : MeetDropDownCell? = tableView.dequeueReusableCell(withIdentifier: "MeetDropDownCell") as? MeetDropDownCell
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("MeetDropDownCell", owner: self, options: nil)? [0] as? MeetDropDownCell)!
            }
            cell?.selectionStyle = .none
            items = section2
            let theMenuItem = items[indexPath.item]
            cell?.titleLabel.text = theMenuItem.Name
            cell?.meetDropDownAction.addTarget(self, action: #selector(dropdownClicked), for: .touchUpInside)
            let meetObj = self.meetingTypeArray?.filter({$0 == self.topMeetingTpe})
            if let index = self.meetingTypeArray?.firstIndex(where: { $0 == self.topMeetingTpe}) {
                selectedRowIrem = index
            }
            cell?.meetTextField.text = meetObj?.first
            if self.isAttendedMeeting {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            return cell ??  UITableViewCell()
        }
        
        if indexPath.section == 2 {
            var cell : MeetDropDownCellSubType? = tableView.dequeueReusableCell(withIdentifier: "MeetDropDownCellSubType") as? MeetDropDownCellSubType
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("MeetDropDownCellSubType", owner: self, options: nil)? [0] as? MeetDropDownCellSubType)!
            }
            cell?.selectionStyle = .none
            cell?.titleLabel.text = "Meeting SubType"
            cell?.meetDropDownAction.addTarget(self, action: #selector(dropdownClickedSubType), for: .touchUpInside)
            let meetObj = self.meetingStatusArraySub?.filter({$0 == self.topMeetingTpeSub})
            if let index = self.meetingStatusArraySub?.firstIndex(where: { $0 == self.topMeetingTpeSub}) {
                selectedRowIremStatusSub = index
            }
            cell?.meetTextField.text = meetObj?.first ?? self.topMeetingTpeSub
            if self.isAttendedMeeting {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            return cell ?? UITableViewCell()
        }
        
        if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MeetDropDownCell", for: indexPath) as! MeetDropDownCell
            cell.selectionStyle = .none
            items = section3
            let theMenuItem = items[indexPath.item]
            cell.titleLabel.text = theMenuItem.Name
            
            let img = UIImage(named: "baseline_date_range_black_24pt")?.withRenderingMode(.alwaysTemplate)
            cell.meetTextField.dropDownButton.setBackgroundImage(img, for: .normal)
            cell.meetTextField.dropDownButton.tintColor = UIColor.MyWall.appColor
            cell.meetDropDownAction.addTarget(self, action: #selector(calnderClicked), for: .touchUpInside)
            
            if self.timeUpdate { //date chnaged
                cell.meetTextField.text = self.topMeetingDate
            }else{
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd"
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                let date: Date? = dateFormatterGet.date(from: self.topMeetingDate ?? "")
                let dateModify = dateFormatter.string(from: date!)
                cell.meetTextField.text = dateModify
            }
            
            if self.isAttendedMeeting {
                cell.isUserInteractionEnabled = false
            }else{
                cell.isUserInteractionEnabled = true
            }
            return cell
        }
        
        if indexPath.section == 4 {
            var cell : MeetDropDownCell? = tableView.dequeueReusableCell(withIdentifier: "MeetDropDownCell") as? MeetDropDownCell
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("MeetDropDownCell", owner: self, options: nil)? [0] as? MeetDropDownCell)!
            }
            cell?.selectionStyle = .none
            items = section4
            let theMenuItem = items[indexPath.item]
            cell?.titleLabel.text = theMenuItem.Name
            
            let img = UIImage(named: "round_access_time_black_24pt")?.withRenderingMode(.alwaysTemplate)
            cell?.meetTextField.dropDownButton.setBackgroundImage(img, for: .normal)
            cell?.meetTextField.dropDownButton.tintColor = UIColor.MyWall.appColor
            cell?.meetDropDownAction.addTarget(self, action: #selector(startTimeClicked), for: .touchUpInside)

            if self.startTimeUpdate {
                cell?.meetTextField.text = self.topMeetingStartTime
            }else{
                let startTimeStr = self.topMeetingStartTime ?? ""
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm:ss"
                let dateFromStr = dateFormatter.date(from: startTimeStr)!
                dateFormatter.amSymbol = "am"
                dateFormatter.pmSymbol = "Pm"
                dateFormatter.dateFormat = "HH:mm a"
                let timeFormatter = DateFormatter()
                timeFormatter.dateStyle = .none
                timeFormatter.timeStyle = .short
                let Date12 = timeFormatter.string(from: dateFromStr)
                cell?.meetTextField.text = Date12
            }
            
            if self.isAttendedMeeting {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            
            
            return cell ?? UITableViewCell()
        }
        
        if indexPath.section == 5 {
            var cell : MeetDropDownCell? = tableView.dequeueReusableCell(withIdentifier: "MeetDropDownCell") as? MeetDropDownCell
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("MeetDropDownCell", owner: self, options: nil)? [0] as? MeetDropDownCell)!
            }
            cell?.selectionStyle = .none
            items = section5
            let theMenuItem = items[indexPath.item]
            cell?.titleLabel.text = theMenuItem.Name
            
            let img = UIImage(named: "round_access_time_black_24pt")?.withRenderingMode(.alwaysTemplate)
            cell?.meetTextField.dropDownButton.setBackgroundImage(img, for: .normal)
            cell?.meetTextField.dropDownButton.tintColor = UIColor.MyWall.appColor
            cell?.meetDropDownAction.addTarget(self, action: #selector(endTimeClicked), for: .touchUpInside)
            if self.endTimeUpdate {
                cell?.meetTextField.text = self.topMeetingEndTime ?? ""
                
            }else{
                let startTimeStr = self.topMeetingEndTime ?? ""
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm:ss"
                let dateFromStr = dateFormatter.date(from: startTimeStr)!
                dateFormatter.amSymbol = "am"
                dateFormatter.pmSymbol = "Pm"
                dateFormatter.dateFormat = "HH:mm a"
                let timeFormatter = DateFormatter()
                timeFormatter.dateStyle = .none
                timeFormatter.timeStyle = .short
                let Date12 = timeFormatter.string(from: dateFromStr)
                cell?.meetTextField.text = Date12
                
            }
            
            
            if self.isAttendedMeeting {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            
            return cell ?? UITableViewCell()
        }
        
        if indexPath.section == 6 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextEditCell", for: indexPath) as! TextEditCell
            cell.selectionStyle = .none
            items = section6
            let theMenuItem = items[indexPath.item]
            cell.titleLabel.text = theMenuItem.Name
            cell.titleDescription.tag = 2
            cell.titleDescription.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                                            for: UIControl.Event.editingChanged)
            cell.titleDescription.delegate = self
            cell.titleDescription.placeholder = "Enter Address"
            cell.titleDescription.text = addressString

            if self.isAttendedMeeting {
                cell.isUserInteractionEnabled = false
            }else{
                cell.isUserInteractionEnabled = true
            }
            
            return cell
        }
        if indexPath.section == 7 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TextEditCell", for: indexPath) as! TextEditCell
            cell.selectionStyle = .none
            items = section7
            let theMenuItem = items[indexPath.item]
            cell.titleLabel.text = theMenuItem.Name
            cell.titleDescription.tag = 3
            cell.titleDescription.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                                            for: UIControl.Event.editingChanged)
            cell.titleDescription.delegate = self
            cell.titleDescription.placeholder = "Enter Department"
            cell.titleDescription.text = departmentString
            
            
            if self.isAttendedMeeting {
                cell.isUserInteractionEnabled = false
            }else{
                cell.isUserInteractionEnabled = true
            }
            
            return cell
        }
        
        if indexPath.section == 8 {
            var cell : MeetDropDownCell? = tableView.dequeueReusableCell(withIdentifier: "MeetDropDownCell") as? MeetDropDownCell
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("MeetDropDownCell", owner: self, options: nil)? [0] as? MeetDropDownCell)!
            }
            cell?.selectionStyle = .none
            items = section8
            let theMenuItem = items[indexPath.item]
            cell?.titleLabel.text = theMenuItem.Name
            cell?.meetDropDownAction.addTarget(self, action: #selector(dropdownClickedStatus), for: .touchUpInside)
            cell?.meetTextField.text = "Scheduled"
            
            let meetObj = self.meetingStatusArray?.filter({
                $0 == self.meetingTypeStatusString
            }
            )
            if let index = self.meetingStatusArray?.firstIndex(where: { $0 == self.meetingTypeStatusString}) {
                selectedRowIremStatus = index
            }
            cell?.meetTextField.text = meetObj?.first
            
            
            if self.isAttendedMeeting {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            
            return cell ?? UITableViewCell()
        }
        
        if indexPath.section == 9 {
            var cell : MomCell? = tableView.dequeueReusableCell(withIdentifier: "MomCell") as? MomCell
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("MomCell", owner: self, options: nil)? [0] as? MomCell)!
            }
            cell?.isUserInteractionEnabled = true

            cell?.selectionStyle = .none
            cell?.titleLabel.text = "MOM Staus"
            cell?.meetDropDownAction.addTarget(self, action: #selector(dropdownClickedMOM), for: .touchUpInside)
            let meetObj = self.momsArray?.filter({
                $0 == self.momStatusString
            }
            )
            if let index = self.momsArray?.firstIndex(where: { $0 == self.momStatusString}) {
                selectedRowIremStatus = index
            }
            cell?.meetTextField.text = meetObj?.first ?? self.momStatusString
            if (cell?.meetTextField.text ?? "").isEmpty {
                self.momStatusString = "Approved"
                cell?.meetTextField.text = self.momStatusString
            }
            
            if self.isMomFulfilled {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            
            return cell ?? UITableViewCell()
        }
        
        if indexPath.section == 10 {
            var cell : MeetDropDownCell? = tableView.dequeueReusableCell(withIdentifier: "MeetDropDownCell") as? MeetDropDownCell
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("MeetDropDownCell", owner: self, options: nil)? [0] as? MeetDropDownCell)!
            }
            cell?.selectionStyle = .none
            
            items = section9
            let theMenuItem = items[indexPath.item]
            cell?.titleLabel.text = theMenuItem.Name
            let img = UIImage(named: "forwardArrow")?.withRenderingMode(.alwaysTemplate)
            cell?.meetTextField.dropDownButton.setBackgroundImage(img, for: .normal)
            cell?.meetTextField.dropDownButton.tintColor = UIColor.MyWall.appColor
            cell?.meetTextField.placeholder = ""
            cell?.meetTextField.text = self.parentMeetingTitle
            cell?.meetDropDownAction.addTarget(self, action: #selector(gtoAllMeetings), for: .touchUpInside)
            
            if self.isAttendedMeeting {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            
            return cell ?? UITableViewCell()
        }
        
        if indexPath.section == 11 {
            //save view
            var cell : SaveUpdatedCell? = tableView.dequeueReusableCell(withIdentifier: "SaveUpdatedCell") as? SaveUpdatedCell
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("SaveUpdatedCell", owner: self, options: nil)? [0] as? SaveUpdatedCell)!
            }
            cell?.selectionStyle = .none
            
            cell?.cancel.addTarget(self, action: #selector(cancelAcrtion), for: .touchUpInside)
            cell?.save.addTarget(self, action: #selector(saveMettingInfo), for: .touchUpInside)
            
            return cell ?? UITableViewCell()
            
        }
        
        if indexPath.section == 12 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddParticipantCell", for: indexPath) as! AddParticipantCell
            cell.selectionStyle = .none
            items = section10
            let theMenuItem = items[indexPath.item]
            cell.addEventButton.setTitle(theMenuItem.Name, for: .normal)
            cell.addEventButton.addTarget(self, action: #selector(addParticipantsItem), for:.touchUpInside)
            
            
            if self.isAttendedMeeting {
                cell.isUserInteractionEnabled = false
            }else{
                cell.isUserInteractionEnabled = true
            }
            
            return cell
        }
        
        if indexPath.section == 13 {
            var cell: UpdateParticipantInfoCell? = (tableView.dequeueReusableCell(withIdentifier: "UpdateParticipantInfoCell") as? UpdateParticipantInfoCell)!
            if(cell == nil) {
                cell = (Bundle.main.loadNibNamed("UpdateParticipantInfoCell", owner: self, options: nil)? [0] as? UpdateParticipantInfoCell)!
            }
            cell?.selectionStyle = .none
            let eachObj = self.participantModelArray[indexPath.row]
            cell?.nameDescLabel.text = eachObj.name
            cell?.phoneDescLabel.text = eachObj.phone
            cell?.departmentDescLabel.text = eachObj.dept
            cell?.designationdesclabel.text = eachObj.designation
            cell?.attendanceStatus.text = eachObj.attendanceStatus
            
            let status =  eachObj.attendanceStatus
            if status?.contains("Attended") == true || status?.contains("Committed") == true  {
                cell?.attendanceStatus.textColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
            }
            else if status?.contains("Attended") == true || status?.contains("Scheduled") == true  {
                cell?.attendanceStatus.textColor = #colorLiteral(red: 0.1254901961, green: 0.2745098039, blue: 0.9607843137, alpha: 1)
            }
            else{
                cell?.attendanceStatus.textColor = #colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1)
            }
            cell?.closeParticiBtn.tag = indexPath.row
            cell?.closeParticiBtn.isHidden = true
            
            if self.isAttendedMeeting {
                cell?.isUserInteractionEnabled = false
            }else{
                cell?.isUserInteractionEnabled = true
            }
            
            return cell ??  UITableViewCell()
        }
        
        if indexPath.section == 14 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddAgendaPlusCell", for: indexPath) as! AddAgendaPlusCell
            cell.selectionStyle = .none
            items = section11
            let theMenuItem = items[indexPath.item]
            cell.addAgendaBtn.setTitle(theMenuItem.Name, for: .normal)
            cell.addAgendaBtn.addTarget(self, action: #selector(addAgendaItem), for:.touchUpInside)
            return cell
        }
        
        if indexPath.section == 15 {
            
            if self.isFromAgenda {
                var cell: AddAgendaCell? = (tableView.dequeueReusableCell(withIdentifier: "AddAgendaCell") as? AddAgendaCell)!
                if(cell == nil) {
                    cell = (Bundle.main.loadNibNamed("AddAgendaCell", owner: self, options: nil)? [0] as? AddAgendaCell)!
                }
                cell?.selectionStyle = .none
                cell?.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                let eachObj = self.agendaAmodelArray[indexPath.row]
                cell?.agendaTextLabel.text = eachObj.agendaTitle
                cell?.agendaTextLabel.adjustsFontSizeToFitWidth = true
                cell?.agendaCloseBtn.tag = indexPath.row
                cell?.agendaCloseBtn.addTarget(self, action: #selector(removeAgenda(sender:)), for:.touchUpInside)
                if self.showAddParticipantFuture {
                    cell?.agendaCloseBtn.isHidden = false
                }else{
                    cell?.agendaCloseBtn.isHidden = true
                }
                
                if self.isAttendedMeeting {
                    cell?.isUserInteractionEnabled = false
                }else{
                    cell?.isUserInteractionEnabled = true
                }
                
                return cell ??  UITableViewCell()
            }
            
            if self.isFromNotes {
                var cell: ViewNoteCell? = (tableView.dequeueReusableCell(withIdentifier: "ViewNoteCell") as? ViewNoteCell)!
                if(cell == nil) {
                    cell = (Bundle.main.loadNibNamed("ViewNoteCell", owner: self, options: nil)? [0] as? ViewNoteCell)!
                }
                cell?.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                cell?.selectionStyle = .none
                let eachObj = self.noteModelArray[indexPath.row]
                cell?.noteTitle.text = eachObj.note
                cell?.xreatedVoteUser.text = eachObj.username
                
                return cell ??  UITableViewCell()
            }
            
            if self.isfromMom {
                var cell: MomSegCell? = (tableView.dequeueReusableCell(withIdentifier: "MomSegCell") as? MomSegCell)!
                if(cell == nil) {
                    cell = (Bundle.main.loadNibNamed("MomSegCell", owner: self, options: nil)? [0] as? MomSegCell)!
                }
                cell?.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                cell?.selectionStyle = .none
                let eachObj = self.momModelArray[indexPath.row]
                cell?.agendaTextLabel.text = eachObj.minute
                cell?.momStatusLabel.text = eachObj.status
                cell?.momStatusLabel.textColor = #colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1)
                
                cell?.agendaCloseBtn.tag = indexPath.row
                cell?.agendaCloseBtn.addTarget(self, action: #selector(deleteMOM(sender:)), for:.touchUpInside)
                cell?.agendaCloseBtn.isHidden = false
                
                if self.isMomFulfilled || self.isMomApproved {
                    cell?.isUserInteractionEnabled = false
                    cell?.agendaCloseBtn.isHidden = true
                }else{
                    cell?.isUserInteractionEnabled = true
                    cell?.agendaCloseBtn.isHidden = false
                }
                
                return cell ??  UITableViewCell()
            }
            
        }
            return cell
        }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 13 {
            if self.showAddParticipantFuture == false{
                return
            }
            //Update Partiticpant
            var tempFrame  = updateParticipantVCObj.view.frame
            tempFrame.size.width = self.view.frame.size.width - 40
            updateParticipantVCObj.view.frame = tempFrame
            popupVC = PopupViewController(contentController: updateParticipantVCObj, popupWidth: tempFrame.size.width, popupHeight: tempFrame.size.height)
            updateParticipantVCObj.meetingEachEventInfo = self.meetingEachEventInfo
            let eachObj = self.participantModelArray[indexPath.row]
            popupVC.cornerRadius = 5
            updateParticipantVCObj.updateID = eachObj.id
            updateParticipantVCObj.attendanceStatus = eachObj.attendanceStatus
            updateParticipantVCObj.UpdateIno()
            updateParticipantVCObj.delegate = self
            updateParticipantVCObj.nameTextfiled.text = eachObj.name
            updateParticipantVCObj.phoneTextfuled.text = eachObj.phone
            updateParticipantVCObj.departmentTextfiled.text = eachObj.dept
            updateParticipantVCObj.designationTextfiled.text = eachObj.designation
            present(popupVC, animated: true, completion: nil)

            
            
        }
        
        if indexPath.section == 15 {
            if self.showAddParticipantFuture == true {
                if self.isFromAgenda {
                    var tempFrame  = updateSegDVC.view.frame
                    tempFrame.size.width = self.view.frame.size.width - 40
                    updateSegDVC.view.frame = tempFrame
                    popupVCUpadate = PopupViewController(contentController: updateSegDVC, popupWidth: tempFrame.size.width, popupHeight: tempFrame.size.height)
                    let eachObj = self.agendaAmodelArray[indexPath.row]
                    updateSegDVC.eachObj = eachObj
                    updateSegDVC.UpdateFileds()
                    updateSegDVC.delegate = self
                    popupVCUpadate.cornerRadius = 5
                    present(popupVCUpadate, animated: true, completion: nil)
                }
            }else{
                //Update agenda
                if self.isfromMom {
                    var tempFrame  = updateMomVCObj.view.frame
                    tempFrame.size.width = self.view.frame.size.width - 40
                    updateMomVCObj.view.frame = tempFrame
                    popupVCUpadateMOM = PopupViewController(contentController: updateMomVCObj, popupWidth:tempFrame.size.width, popupHeight: tempFrame.size.height)
                    updateMomVCObj.meetingEachEventInfo = self.meetingEachEventInfo
                    let eachObj = self.momModelArray[indexPath.row]
                    updateMomVCObj.momStatusString = eachObj.status
                    updateMomVCObj.eachObj = eachObj
                    updateMomVCObj.UpdateFileds()
                    updateMomVCObj.delegate = self
                    popupVCUpadateMOM.cornerRadius = 5
                    present(popupVCUpadateMOM, animated: true, completion: nil)
                }
            }
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 11 {
            return 80
        }
        if indexPath.section == 12 {
            return 50
        }
        if indexPath.section == 12 {
            return 50
        }
        if indexPath.section == 13 {
            return 140
        }
        if indexPath.section == 14 {
            return 50
        }
        if indexPath.section == 15 {
            if self.isFromAgenda {
                return 60
            }
            if self.isFromNotes {
                return 80
            }
            return 80
        }
        return 60
        
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 12 {
            return "Participants"
        }
        return ""
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 12 || section == 14 {
            return 50
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 14 {
            let headerView : AgendaOptionView = AgendaOptionView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            headerView.delegate = self
            if self.isFromAgenda {
                headerView.segmentedControl.selectedSegmentIndex = 0
            }
            if self.isfromMom {
                headerView.segmentedControl.selectedSegmentIndex = 1
            }
            if self.isFromNotes {
                headerView.segmentedControl.selectedSegmentIndex = 2
            }
            return headerView
        }else{
            return nil
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        if section == 12 {
            view.tintColor = UIColor.MyWall.appColor
            let header = view as! UITableViewHeaderFooterView
            header.textLabel?.textColor = UIColor.white
        }
    }
    
    
    @objc func closeParticipant(sender:UIButton){
        
    }
    
    
    
    
}


extension UpdateMeetingVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}



extension UpdateMeetingVC : UpdateSegVCCDelegate
{
    func UpdateSegVCcancelButtonTapped()
    {
        popupVCUpadate.dismiss(animated: true, completion: nil)

    }
    
    func updateAgendaTapped(){
        popupVCUpadate.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            self.refreshMettingInfo()
        }
    }

    
}

extension UpdateMeetingVC : UpdateParticipantVCDDelegate
{
    func cancelButtonUpdateTapped(){
        updateParticipantVCObj.dismiss(animated: true, completion: nil)
    }
    
    func UpddateParticipantAction() {
        updateParticipantVCObj.dismiss(animated: true, completion: nil)
        DispatchQueue.main.async {
            self.refreshMettingInfo()
        }
    }
    
}


extension UpdateMeetingVC : AgendaOptionViewDelegate
{
    func addAgenda() {
        
        self.isFromAgenda  = true
        self.isfromMom = false
        self.isFromNotes = false
        self.tableview.reloadSections([13,14,15], with: .none)
        self.tableview.reloadData()

        
    }
    
    func addMom() {
        self.isFromAgenda  = false
        self.isfromMom = true
        self.isFromNotes = false
        self.getMomObjects()

    }
    
    func addNotes() {
        self.isFromAgenda  = false
        self.isfromMom = false
        self.isFromNotes = true
        self.getNotes()

        
    }
    
    
    
}


extension UpdateMeetingVC : UpdateMomVCDelegate
{
    func cancelFromMOM() {
        popupVCUpadateMOM.dismiss(animated: true, completion: nil)

    }
    
    func UpdateFromMom() {
        popupVCUpadateMOM.dismiss(animated: true, completion: nil)
        self.isFromAgenda  = false
        self.isfromMom = true
        self.isFromNotes = false
        self.getMomObjects()
    }
    
    
}


extension UpdateMeetingVC {
    
    
    func getNotes()
    {
        guard let token:String = UserManager.sharedInstance.currentUser?.accessToken else {
            return
        }
        
        guard let meetingID:String = self.meetingEachEventInfo?.meetingInfo.id else {
            return
        }
        let catUrl : String  =  myHelper.getAllNOtes! + meetingID
        activityView.displayLoader()
        let parameters: [String: String] = ["access_token":token]
        ApiService.sharedManager.getServiceWithoutToken(catUrl, parameters) { (urlData) in
            self.activityView.dismissLoader()
            do {
                let rootDicnew = try JSONDecoder().decode(NoteView.self, from: urlData)
                self.noteModelArray = rootDicnew
                self.tableview.reloadSections([13,14,15], with: .none)

            }
            catch (let error) {
                self.activityView.dismissLoader()
                do {
                    self.view.makeToast(error.localizedDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "", subTitle: error.localizedDescription)
                }
            }
            
        }
        failure: { (errorString) in
            self.activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
        
    }
    
    
    func deleteMOMAlert(sender:UIButton)
    {
        let eachObj = self.momModelArray[sender.tag]
        guard let curID :String = eachObj.id else {
            return
        }
        
        let momDict  : [String : Any] = ["id":  curID ,
                                         "minute":eachObj.minute ?? "" ,
                                         "status":"",
                                         "createdBy":eachObj.createdBy ?? "",
                                         "updatedBy":eachObj.createdBy ?? "",
                                         "meetingObjId":eachObj.meetingObjID ?? ""]
        
        let momParams : [String : Any] = ["mom" : momDict,
                                          "username":eachObj.createdBy ?? "",
                                          "minuteNotes":"",
        ]
        
        let InputUrl = myHelper.deleteMOM
        let activityView = MyActivityView()
        activityView.displayLoader()

        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, momParams, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.getMomObjects()
                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }


    }
    
    @objc func deleteMOM(sender:UIButton){
        let alert = UIAlertController(title: "", message: "Are you sure you want to delete", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {_ in
            self.isFromAgenda = false
            self.isfromMom = false
            self.isfromMom = true
            
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            action in
            DispatchQueue.main.async {
                self.deleteMOMAlert(sender: sender)
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
    }

    func removeAgendaAlert(sender:UIButton){
        if !self.isFromAgenda {
            return
        }
        let eachObj = self.agendaAmodelArray[sender.tag]
        let agendaDict  : [String : String] = ["id": eachObj.id ,
                                               "agendaTitle":eachObj.agendaTitle,
                                               "createdBy":eachObj.createdBy ?? "",
                                               "updatedBy":eachObj.createdBy ?? "",
                                               "meetingObjId":eachObj.meetingObjID]
        
        var agendaArray = [[String:String]]()
        agendaArray.append(agendaDict)
        
        let agendasObj : [String : Any] = ["agendas" : agendaArray,
                                           "createdByName":eachObj.createdBy,
                                "updatedTime":"",
                                "meetingObjId":eachObj.meetingObjID,
                                "notes": ""]
        
        let InputUrl = myHelper.deleteAgenda
        let activityView = MyActivityView()
        activityView.displayLoader()

        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, agendasObj, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.refreshMettingInfo()
                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }

    }
    
    
    @objc func removeAgenda(sender:UIButton){
        
        let alert = UIAlertController(title: "", message: "Are you sure you want to delete", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {_ in
            
            self.isFromAgenda = true
            self.isfromMom = false
            self.isfromMom = false
            
        }))
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {
            action in
            DispatchQueue.main.async {
                self.removeAgendaAlert(sender: sender)
            }
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    func getMomObjects()
    {
        guard let token:String = UserManager.sharedInstance.currentUser?.accessToken else {
            return
        }
        
        guard let meetingID:String = self.meetingEachEventInfo?.meetingInfo.id else {
            return
        }
        let catUrl : String  =  myHelper.getAllMoms! + meetingID
        activityView.displayLoader()
        let parameters: [String: String] = ["access_token":token]
        ApiService.sharedManager.getServiceWithoutToken(catUrl, parameters) { (urlData) in
            self.activityView.dismissLoader()
            do {
                let rootDicnew = try JSONDecoder().decode(MOMViewModel.self, from: urlData)
                self.momModelArray = rootDicnew
                self.tableview.reloadSections([13,14,15], with: .none)
            }
            catch (let error) {
                self.activityView.dismissLoader()
                do {
                    self.view.makeToast(error.localizedDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "", subTitle: error.localizedDescription)
                }
            }
            
        }
        failure: { (errorString) in
            self.activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
        
    }
    
    
    
}








struct NoteViewElement: Codable {
    let id, note, username: String?
    let momObjID: String?
    let meetingObjID: String?

    enum CodingKeys: String, CodingKey {
        case id, note, username
        case momObjID = "momObjId"
        case meetingObjID = "meetingObjId"
    }
}

typealias NoteView = [NoteViewElement]


// MARK: - MOMViewModelElement
struct MOMViewModelElement: Codable {
    let id, minute, status, createdBy: String?
    let updatedBy: String?
    let meetingObjID: String?

    enum CodingKeys: String, CodingKey {
        case id, minute, status, createdBy, updatedBy
        case meetingObjID = "meetingObjId"
    }
}

typealias MOMViewModel = [MOMViewModelElement]
