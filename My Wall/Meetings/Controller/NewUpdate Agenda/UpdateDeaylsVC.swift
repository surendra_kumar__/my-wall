//
//  UpdateDeaylsVC.swift
//  My Wall
//
//  Created by SURENDRA on 05/02/21.
//

import UIKit


protocol UpdateSegVCCDelegate : NSObject {
    func UpdateSegVCcancelButtonTapped()
    func updateAgendaTapped()
    
}


class UpdateDeaylsVC: UIViewController {

    @IBOutlet weak var UpdateTitle: UILabel!
    weak var delegate : UpdateSegVCCDelegate?
    @IBOutlet weak var updateTitleName: HSUnderLineTextField!
    @IBOutlet weak var UpdateNotes: HSUnderLineTextField!
    
    var eachObj : Agenda?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        updateTitleName.text = self.eachObj?.agendaTitle

    }


    @IBAction func cancelTapped(_ sender: Any) {
        
        self.delegate?.UpdateSegVCcancelButtonTapped()
    }
    
    func UpdateFileds()
    {
        updateTitleName.text = self.eachObj?.agendaTitle
        UpdateNotes.setCustomPlaceHolder("Notes ", fontSize: 16, color: .lightGray)
        UpdateNotes.text = ""
    }
    
    func validateFields() -> Bool {
        var validUserName: Bool = true
        var validPassword: Bool = true

        if let userName = updateTitleName.text {
            validUserName = userName.utf8.count > 0
            if !validUserName {
                updateTitleName.displayErrorPlaceholder("Please enter Name")
                updateTitleName.displayError()
            }
        }
        if let userPassword = UpdateNotes.text {
            validPassword = userPassword.utf8.count > 0
            if !validPassword {
                UpdateNotes.displayErrorPlaceholder("Please enter Note")
                UpdateNotes.displayError()
            }
        }
        
        return validUserName && validPassword
    }
    
    @IBAction func saveTapeed(_ sender: Any) {
        guard validateFields() else {return}
        UpdateNotes.clearError()

        let agendaDict  = ["id": self.eachObj?.id ,
                                               "agendaTitle":updateTitleName.text ?? "",
                                               "createdBy":self.eachObj?.createdBy,
                                               "updatedBy":self.eachObj?.createdBy,
                                               "meetingObjId":self.eachObj?.meetingObjID]
        
        var agendaArray = [[String:Any]]()
        agendaArray.append(agendaDict as [String : Any])
        
        let agendasObj : [String : Any] = ["agendas" : agendaArray,
                                           "createdByName":self.eachObj?.createdBy ?? "",
                                "updatedTime":"",
                                "meetingObjId":self.eachObj?.meetingObjID ?? "",
                                "notes": UpdateNotes.text ?? ""]
        
        let InputUrl = myHelper.updateAgenda
        let activityView = MyActivityView()
        activityView.displayLoader()

        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, agendasObj, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.delegate?.updateAgendaTapped()
                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }

        
        
    }
        
    

}
