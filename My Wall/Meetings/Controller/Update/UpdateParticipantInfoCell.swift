//
//  UpdateParticipantInfoCell.swift
//  My Wall
//
//  Created by SURENDRA on 06/02/21.
//

import UIKit

class UpdateParticipantInfoCell: UITableViewCell {

    
    @IBOutlet weak var nameDescLabel: UILabel!
    @IBOutlet weak var phoneDescLabel: UILabel!
    @IBOutlet weak var departmentDescLabel: UILabel!
    @IBOutlet weak var designationdesclabel: UILabel!
    @IBOutlet weak var attendanceStatus: UILabel!

    
    @IBOutlet weak var closeParticiBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
