//
//  UpdateParticipantVC.swift
//  My Wall
//
//  Created by SURENDRA on 06/02/21.
//

import UIKit


protocol UpdateParticipantVCDDelegate : NSObject {
    func cancelButtonUpdateTapped()
    func UpddateParticipantAction()

}



class UpdateParticipantVC: UIViewController {

    var selectedRowIrem =  3

    @IBOutlet weak var nameTextfiled: HSUnderLineTextField!
    @IBOutlet weak var phoneTextfuled: HSUnderLineTextField!
    @IBOutlet weak var departmentTextfiled: HSUnderLineTextField!
    @IBOutlet weak var designationTextfiled: HSUnderLineTextField!
    @IBOutlet weak var notesTextFiled: HSUnderLineTextField!
    var attendanceStatus : String? = "Scheduled"
    
    weak var delegate : UpdateParticipantVCDDelegate?
    
    var meetingEachEventInfo : ViewMeetingInfoModel? = nil
    @IBOutlet weak var attTextFiled: ReasonFieldDropDown!
    
    @IBOutlet weak var aarButton: UIButton!
    var meetingTypeArray : [String]? = []
    var updateID : String? = ""

    
    let chooseDropDownMeetType = DropDown()
    lazy var dropDowns: [DropDown] = {
        return [
            self.chooseDropDownMeetType
        ]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        attTextFiled.layer.borderWidth = 0.5;
        attTextFiled.layer.borderColor = UIColor.lightGray.cgColor
        attTextFiled.layer.cornerRadius = 5
        attTextFiled.clipsToBounds = true
        notesTextFiled.setCustomPlaceHolder("Notes :", fontSize: 16, color: .lightGray)

//        meetingTypeArray = ["Abscent","Attended","Not Attending","Scheduled"]

        // Do any additional setup after loading the view.
    }

    func UpdateIno()
    {
        self.meetingTypeArray = self.meetingEachEventInfo?.attendanceStatusList.compactMap { $0.listItem}
        let meetObj = self.meetingTypeArray?.filter({$0 == self.attendanceStatus})
        if let index = self.meetingTypeArray?.firstIndex(where: { $0 == self.attendanceStatus}) {
            selectedRowIrem = index
        }
        self.attTextFiled.text = meetObj?.first ?? self.attendanceStatus

    }
    
    
    func customizeDropDown(_ sender: AnyObject) {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor.MyWall.LightBlue
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        if #available(iOS 11.0, *) {
            appearance.setupMaskedCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        }
    }
    
    @IBAction func AttenTapped(_ sender: Any) {
        
        self.setupChooseDropDown()
        self.customizeDropDown(self)
        chooseDropDownMeetType.show()

        
    }
    
    func setupChooseDropDown() {
        chooseDropDownMeetType.anchorView = attTextFiled
        chooseDropDownMeetType.bottomOffset = CGPoint(x: 0, y: attTextFiled.bounds.height)
        chooseDropDownMeetType.dataSource = self.meetingTypeArray ?? []
        chooseDropDownMeetType.selectRow(selectedRowIrem)
        chooseDropDownMeetType.selectionAction = { [weak self] (index, item) in
            self?.selectedRowIrem = index
            self?.attTextFiled.text = item
            self?.attTextFiled.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
        chooseDropDownMeetType.cancelAction =  {
            self.attTextFiled.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
    }
    
    
    func validateFields() -> Bool {
        var validUserName: Bool = true
        var validPassword: Bool = true
        var validDepartment: Bool = true
        var validDesignation: Bool = true
        var validDesignationNotes: Bool = true

        
        if let userName = nameTextfiled.text {
            validUserName = userName.utf8.count > 0
            if !validUserName {
                nameTextfiled.displayErrorPlaceholder("Please enter Name")
                nameTextfiled.displayError()
            }
        }
        if let userPassword = phoneTextfuled.text {
            validPassword = userPassword.utf8.count > 0
            if !validPassword {
                phoneTextfuled.displayErrorPlaceholder("Please enter Phone Number")
                phoneTextfuled.displayError()
            }
        }
        
        if let userDepartment = departmentTextfiled.text {
            validDepartment = userDepartment.utf8.count > 0
            if !validDepartment {
                departmentTextfiled.displayErrorPlaceholder("Please enter Department")
                departmentTextfiled.displayError()
            }
        }
        
        if let userDesignation = designationTextfiled.text {
            validDesignation = userDesignation.utf8.count > 0
            if !validDesignation {
                designationTextfiled.displayErrorPlaceholder("Please enter Desination")
                designationTextfiled.displayError()
            }
        }
        
        if let noteText = notesTextFiled.text {
            validDesignationNotes = noteText.utf8.count > 0
            if !validDesignationNotes {
                notesTextFiled.displayErrorPlaceholder("Please enter Notes")
                notesTextFiled.displayError()
            }
        }
        
        
        return validUserName && validPassword && validDepartment && validDesignation && validDesignationNotes
    }

    
    @IBAction func saveTapped(_ sender: Any) {
        
        guard validateFields() else {return}
        guard let userName = nameTextfiled.text else {return}
        guard let phone = phoneTextfuled.text else {return}
        guard let department = departmentTextfiled.text else {return}
        guard let designation = designationTextfiled.text else {return}
        let notesText = notesTextFiled.text ?? ""
        let attendanceStatus = attTextFiled.text ?? ""
        
        let participantDict  : [String : String] =
            ["id":self.updateID ?? "" ,
             "name":userName,
             "phone":phone,
             "designation":designation,
             "dept":department,
             "attendanceStatus":attendanceStatus,
             "meetingObjId":self.meetingEachEventInfo?.meetingInfo.id ?? ""]
        let createdBy = UserManager.sharedInstance.currentUserInfomation?.user.username ?? ""

        
        let parameters : [String : Any] = ["participant" : participantDict,
                                "notes":notesText,
                                "userId":createdBy ]

        let InputUrl = myHelper.updateparticipant
        let activityView = MyActivityView()
        activityView.displayLoader()
        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, parameters, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.delegate?.UpddateParticipantAction()
                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
    }
    
    
    @IBAction func cancelTapped(_ sender: Any) {
        
        self.delegate?.cancelButtonUpdateTapped()
        
    }
    
    
}
