//
//  AgendaOptionView.swift
//  My Wall
//
//  Created by SURENDRA on 04/02/21.
//

import UIKit

protocol AgendaOptionViewDelegate : NSObject {
    func addAgenda()
    func addMom()
    func addNotes()
}


class AgendaOptionView: UIView {

    @IBOutlet var view: UIView!

    weak var delegate : AgendaOptionViewDelegate?
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    
    override init(frame: CGRect) {
          super.init(frame: frame)
          nibSetup()
      }
      
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          nibSetup()
      }
      
    private func nibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
        self.segmentedControl.tintColor = #colorLiteral(red: 0.9411764706, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        self.segmentedControl.backgroundColor = #colorLiteral(red: 0.1529411765, green: 0.1960784314, blue: 0.2196078431, alpha: 1)
        
        let font = UIFont.boldSystemFont(ofSize: 18)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)

                                                               
        let titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for:.normal)

        let titleTextAttributes1 = [NSAttributedString.Key.foregroundColor: UIColor.black]
        segmentedControl.setTitleTextAttributes(titleTextAttributes1, for:.selected)



    }

      
      private func loadViewFromNib() -> UIView {
          let bundle = Bundle(for: type(of: self))
          let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
          let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
          return nibView
      }
    
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            self.delegate?.addAgenda()
            break
            
        case 1:
            self.delegate?.addMom()

            break
            
        case 2:
            self.delegate?.addNotes()

            break
            
        default:
            break;
        }
    }

}
