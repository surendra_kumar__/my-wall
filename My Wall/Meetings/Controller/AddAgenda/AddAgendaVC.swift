//
//  AddAgendaVC.swift
//  My Wall
//
//  Created by SURENDRA on 29/01/21.
//

import UIKit

enum segmentType {
    case agendaSeg
    case momSeg
    case noteSeg
}


protocol AddAgendaVCDelegate : NSObject {
    func cancelAgendaVC()
    func sendAgendaObject(agendaObj:[String:String],agendaModel:Agenda)
    func closeCreatioNotesAndRfreshNotes()
    func closeMomandRefresh()
    
}
struct AgendaModel {
    let adendaStr : String
    let createdBy : String
    let updatedBy : String
    let meetingObjId : String
}

class AddAgendaVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    weak var delegate : AddAgendaVCDelegate?
    var segtype : segmentType = .agendaSeg
    
    @IBOutlet weak var segTypeLabel: UILabel!
    
    var IsFromUpdate : Bool = false
    var meetingEachEventInfo : ViewMeetingInfoModel? = nil
    var isFromCreateMeeting : Bool = true


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        textView.toolbarPlaceholder = "Notes "


        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        textView.delegate = self
        textView.textColor = UIColor.lightGray
        
        textView.layer.cornerRadius = 5
        self.textView.layer.shadowColor = UIColor.black.cgColor;
        self.textView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.textView.layer.shadowOpacity = 0.8
        self.textView.layer.shadowRadius = 2.0
        self.textView.layer.masksToBounds = false
//        self.textView.text = "Add Agenda"
        
    }

    @IBAction func cancelTapped(_ sender: Any) {
        self.delegate?.cancelAgendaVC()
    }
    
    
    
    @IBAction func saveTapped(_ sender: Any) {
        if self.textView.textColor?.isEqual(UIColor.lightGray) == true {
            self.textView.displayError()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: "Please enter text", withDuration: 0.5)
            return
        }
        var validText: Bool = true
        if let text = self.textView.text  {
            validText = text.utf8.count > 0
            if !validText {
                self.textView.displayError()
                return
            }
        }
        
        if self.isFromCreateMeeting {
            guard let agendaTitle = self.textView.text else { return }
            self.textView.text = ""
            
            if self.segtype == .agendaSeg {
                let parameters = ["agendaTitle":agendaTitle, "createdBy":"","updatedBy":"","meetingObjId":"","id":""]
                let agenda = Agenda.init(id: "", agendaTitle: agendaTitle, createdBy: "", updatedBy: "", meetingObjID: "")
                self.delegate?.sendAgendaObject(agendaObj: parameters, agendaModel: agenda)
            }
            
            if self.segtype == .momSeg {
                
                
            }
            
            if self.segtype == .noteSeg {
                
                self.createNotes()
            }
        }
        else {
            
            
            if self.segtype == .agendaSeg {
                
                self.createAgenda()
            }
            
            if self.segtype == .momSeg {
                
                self.createMOM()
            }
            
            if self.segtype == .noteSeg {
                self.createNotes()
            }
            
            
        }
        
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    
   

    @objc func createNotes () {
        self.view.endEditing(true)
        guard let agendaTitle = self.textView.text else {
            self.textView.displayError()
            return
        }
        let createdBy = UserManager.sharedInstance.currentUserInfomation?.user.username ?? ""
        let agendaDict  : [String : Any] = ["id": "" ,"note":agendaTitle, "username":createdBy,"momObjId":self.meetingEachEventInfo?.meetingInfo.id ?? "","meetingObjId":self.meetingEachEventInfo?.meetingInfo.id ?? ""]
        let InputUrl = myHelper.createNotesService
        let activityView = MyActivityView()
        activityView.displayLoader()

        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, agendaDict, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.delegate?.closeCreatioNotesAndRfreshNotes()
                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
        
    }
    
    
     func createAgenda () {
        self.view.endEditing(true)
        guard let agendaTitle = self.textView.text else {
            self.textView.displayError()
            return
        }
        let createdBy = UserManager.sharedInstance.currentUserInfomation?.user.username ?? ""
        let dateStr = Date.getCurrentDate()
        
        let agendaDict  : [String : String] = ["id":  "" ,"agendaTitle":agendaTitle, "createdBy":createdBy,"updatedBy":createdBy,"meetingObjId":self.meetingEachEventInfo?.meetingInfo.id ?? ""]
        
        var agendaArray = [[String:String]]()
        agendaArray.append(agendaDict)
        
        let agendasObj : [String : Any] = ["agendas" : agendaArray,
                                "createdByName":createdBy,
                                "updatedTime":dateStr,
                                "meetingObjId":self.meetingEachEventInfo?.meetingInfo.id ?? "",
                                "notes": ""]
        
        
        let InputUrl = myHelper.Createagenda
        let activityView = MyActivityView()
        activityView.displayLoader()
        let agenda = Agenda.init(id: "", agendaTitle: agendaTitle, createdBy: "", updatedBy: "", meetingObjID: "")

        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, agendasObj, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.delegate?.sendAgendaObject(agendaObj: agendaDict, agendaModel: agenda)

                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
        
    }

    
    
    
    func createMOM () {
       self.view.endEditing(true)
       guard let momTtile = self.textView.text else {
           self.textView.displayError()
           return
       }
       let createdBy = UserManager.sharedInstance.currentUserInfomation?.user.username ?? ""
       let dateStr = Date.getCurrentDate()
       
       let agendaDict  : [String : String] = ["id":  "" ,
                                              "minute":momTtile,
                                              "status":"",
                                              "createdBy":createdBy,
                                              "updatedBy":createdBy,
                                              "meetingObjId":self.meetingEachEventInfo?.meetingInfo.id ?? ""]
       
       
       let momParams : [String : Any] = ["mom" : agendaDict,
                               "username":createdBy,
                               "minuteNotes":dateStr,
                               ]
       
       
       let InputUrl = myHelper.createMOM
       let activityView = MyActivityView()
       activityView.displayLoader()

       ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, momParams, success: { (urlData) in
           activityView.dismissLoader()
           do {
               let val = String(data: urlData, encoding: String.Encoding.utf8)
               DispatchQueue.main.async {
                   
                   if ((val?.contains("true")) == true)
                   {
                    self.delegate?.closeMomandRefresh()


                   }
               }
               
           } catch  {
               do {
                   let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                   ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
               }
               catch {
                   ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
               }
           }
           
       })
       { (errorString) in
           activityView.dismissLoader()
           ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
       }
       
   }

    
    
}


extension Date {

 static func getCurrentDate() -> String {

        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy-MM-dd"

        return dateFormatter.string(from: Date())

    }
}
