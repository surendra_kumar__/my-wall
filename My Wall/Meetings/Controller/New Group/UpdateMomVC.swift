//
//  UpdateMomVC.swift
//  My Wall
//
//  Created by SURENDRA on 06/02/21.
//

import UIKit


protocol UpdateMomVCDelegate : NSObject {
    func cancelFromMOM()
    func UpdateFromMom()
    
}

class UpdateMomVC: UIViewController {
    var selectedRowIrem =  1
    var momStatusString : String? = "Draft"
    var momsArray : [String]? = []
    var meetingEachEventInfo : ViewMeetingInfoModel? = nil
    @IBOutlet weak var attTextFiled: ReasonFieldDropDown!

    @IBOutlet weak var UpdateTitle: UILabel!
    @IBOutlet weak var updateTitleName: HSUnderLineTextField!
    @IBOutlet weak var UpdateNotes: HSUnderLineTextField!
    var eachObj : MOMViewModelElement?
    weak var delegate : UpdateMomVCDelegate?

    
    let chooseDropDownMOM = DropDown()
    lazy var dropDownStatusMOM: [DropDown] = {
        return [
            self.chooseDropDownMOM
        ]
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func UpdateFileds()
    {
        updateTitleName.text = self.eachObj?.minute
        UpdateNotes.setCustomPlaceHolder("Notes ", fontSize: 16, color: .lightGray)
        UpdateNotes.text = ""
        self.UpdateInfo()

    }
   
    func UpdateInfo()
    {
        self.momsArray = self.meetingEachEventInfo?.momStatusList.compactMap { $0.listItem}
        let meetObj = self.momsArray?.filter({$0 == self.momStatusString})
        if let index = self.momsArray?.firstIndex(where: { $0 == self.momStatusString}) {
            selectedRowIrem = index
        }
        self.attTextFiled.text = meetObj?.first ?? self.momStatusString
    }
    
    func customizeDropDown(_ sender: AnyObject) {
        let appearance = DropDown.appearance()
        appearance.cellHeight = 60
        appearance.backgroundColor = UIColor(white: 1, alpha: 1)
        appearance.selectionBackgroundColor = UIColor.MyWall.LightBlue
        appearance.cornerRadius = 10
        appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
        appearance.shadowOpacity = 0.9
        appearance.shadowRadius = 25
        appearance.animationduration = 0.25
        appearance.textColor = .darkGray
        if #available(iOS 11.0, *) {
            appearance.setupMaskedCorners([.layerMaxXMaxYCorner, .layerMinXMaxYCorner])
        }
    }
    @IBAction func momTapped(_ sender: Any) {
        self.setupChooseDropDown()
        self.customizeDropDown(self)
        chooseDropDownMOM.show()
    }

    func setupChooseDropDown() {
        chooseDropDownMOM.anchorView = attTextFiled
        chooseDropDownMOM.bottomOffset = CGPoint(x: 0, y: attTextFiled.bounds.height)
        chooseDropDownMOM.dataSource = self.momsArray ?? []
        chooseDropDownMOM.selectRow(selectedRowIrem)
        chooseDropDownMOM.selectionAction = { [weak self] (index, item) in
            self?.selectedRowIrem = index
            self?.attTextFiled.text = item
            self?.attTextFiled.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
        chooseDropDownMOM.cancelAction =  {
            self.attTextFiled.dropDownButton.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        }
    }
        
    @IBAction func cancelTapped(_ sender: Any) {
        
        self.delegate?.cancelFromMOM()
    }
    
    func validateFields() -> Bool {
        var validUserName: Bool = true
        var validPassword: Bool = true

        if let userName = updateTitleName.text {
            validUserName = userName.utf8.count > 0
            if !validUserName {
                updateTitleName.displayErrorPlaceholder("Please enter Name")
                updateTitleName.displayError()
            }
        }
        if let userPassword = UpdateNotes.text {
            validPassword = userPassword.utf8.count > 0
            if !validPassword {
                UpdateNotes.displayErrorPlaceholder("Please enter Note")
                UpdateNotes.displayError()
            }
        }
        
        return validUserName && validPassword
    }
    
    @IBAction func saveTapeed(_ sender: Any) {
        //Update MOM
        guard validateFields() else {return}
        UpdateNotes.clearError()
        let momStatus = attTextFiled.text ?? ""

        let momDict  : [String : Any] = ["id":  self.eachObj?.id ?? "",
                                         "minute":updateTitleName.text ?? "",
                                         "status":momStatus,
                                         "createdBy":self.eachObj?.createdBy ?? "",
                                         "updatedBy":self.eachObj?.createdBy ?? "",
                                         "meetingObjId":self.eachObj?.meetingObjID ?? ""]
        
        
        let momParams : [String : Any] = ["mom" : momDict,
                                          "username":self.eachObj?.createdBy ?? "",
                                          "minuteNotes":UpdateNotes.text ?? "",
        ]
        
        let InputUrl = myHelper.momUpdate
        let activityView = MyActivityView()
        activityView.displayLoader()
        
        ApiService.sharedManager.startPostApiServiceWithBearerTokenWithRawDataCreateMeeting(InputUrl, momParams, success: { (urlData) in
            activityView.dismissLoader()
            do {
                let val = String(data: urlData, encoding: String.Encoding.utf8)
                DispatchQueue.main.async {
                    
                    if ((val?.contains("true")) == true)
                    {
                        self.delegate?.UpdateFromMom()
                    }
                }
                
            } catch  {
                do {
                    let errorModel = try JSONDecoder().decode(ErrorModel.self, from: urlData)
                    ErrorManager.showErrorAlert(mainTitle: errorModel.error, subTitle: errorModel.errorDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "invalid_grant", subTitle: error.localizedDescription)
                }
            }
            
        })
        { (errorString) in
            activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
        
        
        
        
        
    }
    
    
}
