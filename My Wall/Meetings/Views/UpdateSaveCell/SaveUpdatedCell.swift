//
//  SaveUpdatedCell.swift
//  My Wall
//
//  Created by SURENDRA on 05/02/21.
//

import UIKit

class SaveUpdatedCell: UITableViewCell {

    
    @IBOutlet weak var cancel: CustomButton!
    @IBOutlet weak var save: CustomButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
