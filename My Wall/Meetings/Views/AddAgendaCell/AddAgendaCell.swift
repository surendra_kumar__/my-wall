//
//  AddAgendaCell.swift
//  My Wall
//
//  Created by SURENDRA on 30/01/21.
//

import UIKit

class AddAgendaCell: UITableViewCell {
    
    
    @IBOutlet weak var agnedaView: UIView!
    @IBOutlet weak var agendaTextLabel: UILabel!
    @IBOutlet weak var agendaCloseBtn: UIButton!
    
    
     override func prepareForReuse() {
        super.prepareForReuse()
        
        self.agendaTextLabel.text = nil
        
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.agnedaView.layer.shadowColor = UIColor.black.cgColor;
        self.agnedaView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.agnedaView.layer.shadowOpacity = 0.8
        self.agnedaView.layer.shadowRadius = 2.0
        self.agnedaView.layer.masksToBounds = false

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
