//
//  AddAgendaPlusCell.swift
//  My Wall
//
//  Created by SURENDRA on 30/01/21.
//

import UIKit

class AddAgendaPlusCell: UITableViewCell {

    @IBOutlet weak var addAgendaBtn: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.addAgendaBtn.setTitle(nil, for: .normal)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
