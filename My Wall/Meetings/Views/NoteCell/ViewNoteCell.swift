//
//  ViewNoteCell.swift
//  My Wall
//
//  Created by SURENDRA on 06/02/21.
//

import UIKit

class ViewNoteCell: UITableViewCell {

    @IBOutlet weak var noteTitle: UILabel!
    @IBOutlet weak var xreatedVoteUser: UILabel!
    @IBOutlet weak var noteViewBG: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.noteViewBG.layer.shadowColor = UIColor.black.cgColor;
        self.noteViewBG.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.noteViewBG.layer.shadowOpacity = 0.8
        self.noteViewBG.layer.shadowRadius = 2.0
        self.noteViewBG.layer.masksToBounds = false
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
