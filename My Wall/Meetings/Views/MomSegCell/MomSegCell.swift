//
//  MomSegCell.swift
//  My Wall
//
//  Created by SURENDRA on 07/02/21.
//

import UIKit

class MomSegCell: UITableViewCell {

    @IBOutlet weak var agnedaView: UIView!
    @IBOutlet weak var agendaTextLabel: UILabel!
    @IBOutlet weak var agendaCloseBtn: UIButton!
    
    @IBOutlet weak var momStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.agnedaView.layer.shadowColor = UIColor.black.cgColor;
        self.agnedaView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.agnedaView.layer.shadowOpacity = 0.8
        self.agnedaView.layer.shadowRadius = 2.0
        self.agnedaView.layer.masksToBounds = false
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
