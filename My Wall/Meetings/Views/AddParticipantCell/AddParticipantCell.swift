//
//  AddParticipantCell.swift
//  My Wall
//
//  Created by surendra on 28/01/21.
//

import UIKit

class AddParticipantCell: UITableViewCell {

    @IBOutlet weak var addEventButton: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.addEventButton.setTitle(nil, for: .normal)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
