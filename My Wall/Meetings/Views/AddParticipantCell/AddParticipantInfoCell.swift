//
//  AddParticipantInfoCell.swift
//  My Wall
//
//  Created by SURENDRA on 29/01/21.
//

import UIKit

class AddParticipantInfoCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameDescLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneDescLabel: UILabel!
    
    @IBOutlet weak var deprtmentLabel: UILabel!
    @IBOutlet weak var departmentDescLabel: UILabel!
    
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var designationdesclabel: UILabel!
    
    @IBOutlet weak var closeParticiBtn: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.nameDescLabel.text = nil
        self.phoneDescLabel.text = nil
        self.departmentDescLabel.text = nil
        self.designationdesclabel.text = nil

        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
