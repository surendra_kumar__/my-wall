//
//  MomCell.swift
//  My Wall
//
//  Created by SURENDRA on 06/02/21.
//

import UIKit

class MomCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var meetTextField: ReasonFieldDropDown!
    @IBOutlet weak var meetDropDownAction: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.text = ""
        meetTextField.text = ""
        meetDropDownAction.setTitle("", for: .normal)

        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
