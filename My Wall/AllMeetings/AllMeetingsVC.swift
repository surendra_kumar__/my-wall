//
//  AllMeetingsVC.swift
//  My Wall
//
//  Created by SURENDRA on 02/02/21.
//

import UIKit

protocol AllMeetingsVCDelegate : NSObject {
    func sendMeetingObjectIDinfo( meeetingModel : AllMeetingsModelElementElement)
}



class AllMeetingsVC: UIViewController {
    
    
    var CalendarEventsArray = [CalendarEvent]()
    var allMeetingsModelarray : AllMeetingsModel?
    weak var delegate : AllMeetingsVCDelegate?


    lazy var tableview: UITableView = {
        let tableView = UITableView()
        tableView.delegate = (self as UITableViewDelegate)
        tableView.dataSource = (self as UITableViewDataSource)
        tableView.tableFooterView = UIView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    var meetinfLbael : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor.MyWall.appColor
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textAlignment = .center
        return label
    }()
    
    
    
    let activityView = MyActivityView()


    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Meetings"
        self.ConfigureNavBarDetils()
        
        
        view.addSubview(meetinfLbael)
        
        meetinfLbael.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        meetinfLbael.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        meetinfLbael.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        meetinfLbael.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        view.addSubview(tableview)
        tableview.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        tableview.bottomAnchor.constraint(equalTo: meetinfLbael.topAnchor, constant: 0).isActive = true
        tableview.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        tableview.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        
        
        let nibName = UINib(nibName: "AllMeetingsCell", bundle:nil)
        tableview.register(nibName, forCellReuseIdentifier: "AllMeetingsCell")
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = 120

        self.getAllMeetings()
    }
    
    
    func getAllMeetings()
    {
        guard let token:String = UserManager.sharedInstance.currentUser?.accessToken else {
            return
        }
        let catUrl : String =  myHelper.getAllmEttingsUrl!
        activityView.displayLoader()
        let parameters: [String: String] = ["access_token":token]

        ApiService.sharedManager.getServiceWithoutToken(catUrl, parameters) { (urlData) in
            self.activityView.dismissLoader()
            do {
                let rootDicnew = try JSONDecoder().decode(AllMeetingsModel.self, from: urlData)
                if rootDicnew.count > 0 {
                    self.allMeetingsModelarray = rootDicnew
                    DispatchQueue.main.async {
                        self.meetinfLbael.text = "Total No of meetings : " + "\(self.allMeetingsModelarray?.count ?? 0)"
                    }

                    for eachEvent in rootDicnew {
                        var dateMeeting : String = eachEvent.date ?? ""
                        var startTime : String = eachEvent.startTime ?? ""
                        var endTime : String = eachEvent.endTime ?? ""
                        if dateMeeting == "" || startTime == "" || endTime == ""  {
                            continue
                        }
                        let dateFormatterGet1 = DateFormatter()
                        dateFormatterGet1.dateFormat = "yyyy-MM-dd"
                        let dateinyyyy: Date? = dateFormatterGet1.date(from: dateMeeting)
                        dateFormatterGet1.dateFormat = "dd-MM-yyyy"
                        dateMeeting = dateFormatterGet1.string(from: dateinyyyy!)
                        
                        let dateFormatterPM = DateFormatter()
                        dateFormatterPM.dateFormat = "HH:mm:ss"
                        let dateendTime = dateFormatterPM.date(from: startTime)
                        dateFormatterPM.dateFormat = "hh:mm a"
                        startTime = dateFormatterPM.string(from: dateendTime!)

                        let dateFormatterPMend = DateFormatter()
                        dateFormatterPMend.dateFormat = "HH:mm:ss"
                        let dateendTimeend = dateFormatterPMend.date(from: endTime)
                        dateFormatterPMend.dateFormat = "hh:mm a"
                        endTime = dateFormatterPMend.string(from: dateendTimeend!)
                        
                        let dateFrom = "Start" + " : " + dateMeeting + " " + startTime
                        let dateTo  = "End" + " : " + dateMeeting + " " + endTime
                        let completeDate = dateFrom + "   " + dateTo
                        let dateObj : [String : String] = ["dateFrom":completeDate,
                                                           "address":eachEvent.location ?? "",
                                                           "startTime":dateFrom,
                                                           "endTime":dateTo,
                                                           "status":eachEvent.status ?? "",
                                                           "momStatus":eachEvent.momStatus ?? ""
                        ]
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        let meetingIDObj : String? = String(eachEvent.meetingID ?? 0)
                        let date = dateFormatter.date(from: eachEvent.date ?? "")
                        let event : CalendarEvent = CalendarEvent(title: eachEvent.title ?? "", andDate: date, andInfo: dateObj,withMeetingID : meetingIDObj)
                        self.CalendarEventsArray.append(event)
                    }
                    self.tableview.reloadData()
                }
                print("")
            }
            catch (let error) {
                self.activityView.dismissLoader()
                do {
                    self.view.makeToast(error.localizedDescription)
                }
                catch {
                    ErrorManager.showErrorAlert(mainTitle: "", subTitle: error.localizedDescription)
                }
            }
            
        }
        failure: { (errorString) in
            self.activityView.dismissLoader()
            ErrorManager.showErrorAlert(mainTitle: "", subTitle: errorString)
        }
    }


}

extension AllMeetingsVC: UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CalendarEventsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllMeetingsCell", for: indexPath) as! AllMeetingsCell
        let event = self.CalendarEventsArray[indexPath.row];
        cell.ckEventTitle.text = event.title;
        cell.statusDescLabel.text = event.info["status"] as? String
        cell.startTimeLabel.text = event.info["startTime"] as? String
        cell.endTimeLabel.text = event.info["endTime"] as? String
        cell.ckEventPlace.text = event.info["address"] as? String
        cell.ckMomStatusLabel.text = event.info["momStatus"] as? String
        cell.ckEventPlace.adjustsFontSizeToFitWidth = true;
        
        let status =  event.info["status"] as? String;
        if status?.contains("Attended") == true || status?.contains("Committed") == true  {
            cell.statusDescLabel.textColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        }
        else if status?.contains("Attended") == true || status?.contains("Scheduled") == true  {
            cell.statusDescLabel.textColor = #colorLiteral(red: 0.1254901961, green: 0.2745098039, blue: 0.9607843137, alpha: 1)
        }
        else{
            cell.statusDescLabel.textColor = #colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1)
        }
        
        let momStatus =  event.info["momStatus"] as? String;
        if momStatus?.contains("Approved") == true {
            cell.ckMomStatusLabel.textColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
        }
        else{
            cell.ckMomStatusLabel.textColor = #colorLiteral(red: 0.9568627451, green: 0.262745098, blue: 0.2117647059, alpha: 1)
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let event = self.CalendarEventsArray[indexPath.row]
        guard let meetingModel = self.allMeetingsModelarray?[indexPath.row] else { return }
        self.delegate?.sendMeetingObjectIDinfo(meeetingModel: meetingModel)
        self.dismiss(animated: true, completion: nil)
    }

    
}

/*

// MARK: - AllMeetingsModelElementElement
struct AllMeetingsModelElementElement: Codable {
    let id: String
    let meetingID: Int
    var title: String
    let type: String
    let subType: String?
    let status: String
    let date, startTime, endTime, location: String?
    let department: String
    let momStatus: String?
    let createdBy: String?
    let parentMeetingObjID: String?
    let parentMeetingTitle, parentMeetingID: String?

    enum CodingKeys: String, CodingKey {
        case id
        case meetingID = "meetingId"
        case title, type, subType, status, date, startTime, endTime, location, department, momStatus, createdBy
        case parentMeetingObjID = "parentMeetingObjId"
        case parentMeetingTitle
        case parentMeetingID = "parentMeetingId"
    }
}

enum CreatedBy: String, Codable {
    case empty = ""
    case sandeep = "sandeep"
    case sreenivas = "Sreenivas"
    case sysadmin = "sysadmin"
}

enum MOMStatus: String, Codable {
    case draft = "Draft"
    case empty = ""
    case null = "null"
}

enum ParentMeetingObjID: String, Codable {
    case empty = ""
    case the5F8F1B4258834017B402483A = "5f8f1b4258834017b402483a"
    case the5F98268560Fe3E7593Fcf813 = "5f98268560fe3e7593fcf813"
    case the5F98328860Fe3E7593Fcf814 = "5f98328860fe3e7593fcf814"
}




typealias AllMeetingsModel = [AllMeetingsModelElementElement]

*/

struct AllMeetingsModelElementElement: Codable {
    let id: String
    let meetingID: Int
    let title: String?
    let type: String?
    let subType: String?
    let status: String?
    let date, startTime, endTime, location: String?
    let department: String?
    let momStatus: String?
    let createdBy: String?
    let parentMeetingObjID, parentMeetingTitle, parentMeetingID: String?

    enum CodingKeys: String, CodingKey {
        case id
        case meetingID = "meetingId"
        case title, type, subType, status, date, startTime, endTime, location, department, momStatus, createdBy
        case parentMeetingObjID = "parentMeetingObjId"
        case parentMeetingTitle
        case parentMeetingID = "parentMeetingId"
    }
}

enum CreatedBy: String, Codable {
    case createdBySandeep = "Sandeep"
    case empty = ""
    case sandeep = "sandeep"
    case sreenivas = "Sreenivas"
    case sysadmin = "sysadmin"
}

enum MOMStatus: String, Codable {
    case approved = "Approved"
    case draft = "Draft"
    case empty = ""
    case null = "null"
    case preparing = "Preparing"
}


enum SubType: String, Codable {
    case followUp = "Follow-up"
    case main = "Main"
    case subTypeFollowUp = "Follow-Up"
}


typealias AllMeetingsModel = [AllMeetingsModelElementElement]
