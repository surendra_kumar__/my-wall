//
//  AllMeetingsCell.swift
//  My Wall
//
//  Created by SURENDRA on 02/02/21.
//

import UIKit

class AllMeetingsCell: UITableViewCell {

    @IBOutlet weak var ckEventTitle: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var statusDescLabel: UILabel!
    
    @IBOutlet weak var startTimeLabel: UILabel!
    
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var ckMomStatusLabel: UILabel!

    
    @IBOutlet weak var ckEventPlace: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
